int calc_sqrtcov_given_rhos
(int N_pops, int *N_nodes, double *sigma, double *rho_recip, double *rho_conv, 
 double *rho_div, double *rho_chain, 
 double *sqrt_diag, double *sqrt_recip, double *sqrt_conv, 
 double *sqrt_div, double *sqrt_chain);
