#include <iostream>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <gsl/gsl_rng.h>
#include "secorder_gen.hpp"
#include "calc_stats_2p.hpp"
#include "calc_stats_1p.hpp"


using namespace std;



int main(int argc, char *argv[]) {
  
  // optional flags
  int deterministic_seed = 0; // if nonzero, use for random num gen seed

  int N_pops=2;
  //int N_nodes_old[2] = {500,500};
  double p_old[2][2] = {{0.01, 0.01},{0.01,0.01}};
  double alpha_recip_old[2][2] = {{0.8, 0.8},{99,0.8}};
  double alpha_conv_old[2][2][2] = {{{0.7,0.7},{99,0.7}},{{0.7,0.7},{99,0.7}}};
  double alpha_div_old[2][2][2] = {{{0.6,0.6},{0.6,0.6}},{{99,99},{0.6,0.6}}};
  double alpha_chain_old[2][2][2] = {{{0.5,0.5},{0.5,0.5}},{{0.5,0.5},{0.5,0.5}}};
  
  //{{{000,001},{010,011}},{{100,101},{110,111}}}

  int *N_nodes = new int[N_pops];
  double *p = new double[N_pops*N_pops];
  double *alpha_recip = new double[N_pops*N_pops];
  double *alpha_conv_hom = new double[N_pops*N_pops];
  double *cc_conv_mixed = new double[N_pops*N_pops*N_pops];
  double *alpha_div_hom = new double[N_pops*N_pops];
  double *cc_div_mixed = new double[N_pops*N_pops*N_pops];
  double *cc_chain = new double[N_pops*N_pops*N_pops];

  for(int i=0; i<N_pops; i++) {
    N_nodes[i] = 1500;
    for(int j=0; j<N_pops; j++) 
      p[i*N_pops+j] = p_old[0][0]*((i+2*j+i*j+1)% 11);//p_old[i][j];
    for(int j=i; j<N_pops; j++) 
      alpha_recip[i*N_pops+j] = 0.0;//alpha_recip_old[0][0];//alpha_recip_old[i][j];
    for(int j=0; j<N_pops; j++) {
      alpha_conv_hom[i*N_pops+j] = 0.0;//alpha_conv_old[0][0][0];
      alpha_div_hom[i*N_pops+j] = 0.0;//alpha_div_old[0][0][0];
    }
  }
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops-1; j++) 
      for(int k=j+1; k<N_pops; k++) 
	cc_conv_mixed[(i*N_pops+j)*N_pops+k] = 0.0;
  for(int i=0; i<N_pops-1; i++) 
    for(int j=i+1; j<N_pops; j++) 
      for(int k=0; k<N_pops; k++)
	cc_div_mixed[(i*N_pops+j)*N_pops+k] = 0.0;
  for(int i=0; i<N_pops; i++)
    for(int j=0; j<N_pops; j++) 
      for(int k=0; k<N_pops; k++)
	cc_chain[(i*N_pops+j)*N_pops+k] = 0.0;//0.546690169;

  //p[0]=0.0;
  // p[0*N_pops+0]=0.01;
  // p[0*N_pops+1]=0.02;
  // p[1*N_pops+0]=0.03;
  // p[1*N_pops+1]=0.04;
  
  // alpha_conv_hom[0*N_pops+0]=1.0;
  // alpha_conv_hom[0*N_pops+1]=0.9;
  // alpha_div_hom[0*N_pops+0]=0.8;
  // alpha_div_hom[1*N_pops+0]=0.7;
  // cc_conv_mixed[(0*N_pops+0)*N_pops+1] = 0.686355575;
  // cc_div_mixed[(0*N_pops+1)*N_pops+0] = 0.71087773;

  // cc_chain[(0*N_pops+0)*N_pops+0] = 0.507722064;
  // cc_chain[(1*N_pops+0)*N_pops+0] = 0.412340908;
  // cc_chain[(0*N_pops+0)*N_pops+1] = 0.280093134;
  // cc_chain[(1*N_pops+0)*N_pops+1] = 0.151633506;

  // alpha_conv_hom[1*N_pops+0]=1.05;
  // alpha_conv_hom[1*N_pops+1]=0.95;
  // alpha_div_hom[0*N_pops+1]=0.85;
  // alpha_div_hom[1*N_pops+1]=0.75;
  // cc_conv_mixed[(1*N_pops+0)*N_pops+1] = 0.700198878;
  // cc_div_mixed[(0*N_pops+1)*N_pops+1] = 0.729695152;

  // cc_chain[(0*N_pops+1)*N_pops+0] = 0.534100249;
  // cc_chain[(1*N_pops+1)*N_pops+0] = 0.32846692;
  // cc_chain[(0*N_pops+1)*N_pops+1] = 0.201201269;
  // cc_chain[(1*N_pops+1)*N_pops+1] = 0.464608698;

  

  gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);

  int N_nodes_tot=0;
  for(int i=0; i<N_pops; i++) 
    N_nodes_tot += N_nodes[i];

  // set the seed
  // if deterministic_seed, use that for seed
  if(deterministic_seed)
    gsl_rng_set(r,deterministic_seed); 
  // else use time in seconds for the seed
  else
    gsl_rng_set(r, time(NULL));

  
  // generate second order matrix W
  gsl_matrix *W;
  W = secorder_gen(N_pops,N_nodes,p, alpha_recip, 
		   alpha_conv_hom, cc_conv_mixed, 
		   alpha_div_hom, cc_div_mixed,
		   cc_chain, r);

  // if failed to generate a matrix, write error and quit
  if(!W) {
    cerr << "Failed to generate the matrix\n";
    return -1;
  }
 

  // matrix files will be stored in data directory
  // with filename determined by the six input parameters
  mkdir("data",0755);  // make data directory, if it doesn't exist
  char FNbase[200];
  sprintf(FNbase,"_%i_%i_%1.3f_%1.3f_%1.3f_%1.3f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f_%1.1f",
	  N_nodes[0], N_nodes[1],
	  p_old[0][0],p_old[0][1],p_old[1][0],p_old[1][1],
	  alpha_recip_old[0][0], alpha_recip_old[0][1], alpha_recip_old[1][1],
	  alpha_conv_old[0][0][0], alpha_conv_old[0][0][1], alpha_conv_old[0][1][1],
	  alpha_conv_old[1][0][0], alpha_conv_old[1][0][1], alpha_conv_old[1][1][1],
	  alpha_div_old[0][0][0], alpha_div_old[0][0][1], alpha_div_old[0][1][0],
	  alpha_div_old[0][1][1], alpha_div_old[1][1][0], alpha_div_old[1][1][1],
	  alpha_chain_old[0][0][0], alpha_chain_old[0][0][1], 
	  alpha_chain_old[0][1][0], alpha_chain_old[0][1][1],
	  alpha_chain_old[1][0][0], alpha_chain_old[1][0][1],
	  alpha_chain_old[1][1][0], alpha_chain_old[1][1][1]);



  // write matrix to a file
  char FN[200];
  FILE *fhnd;
  strcpy(FN, "data/w");
  strcat(FN, FNbase);
  strcat(FN, ".dat");
  fhnd = fopen(FN, "w");
  if(fhnd==NULL) {
    cerr << "Couldn't open outfile file " << FN << "\n";
    exit(-1);
  }

  
  for(int i=0; i<N_nodes_tot;i++) {
    for(int j=0; j<N_nodes_tot; j++) {
      fprintf(fhnd, "%i ", (int) gsl_matrix_get(W,i,j));
    }
    fprintf(fhnd,"\n");
  }
  fclose(fhnd);



  ////////////////////////////////////////////////////////////
  // Calculate the covariance structure of the matrix 
  // This should approximately agree with the input alphas
  ////////////////////////////////////////////////////////////

  cout << "Testing statistics of matrix...";
  cout.flush();

  if(N_pops!=2) {
    double phat, alphahat_recip, alphahat_conv, alphahat_div,
      alphahat_chain, alphahat_other;
    
    calc_phat_alphahat_1p(W, N_nodes_tot, phat, alphahat_recip, 
			  alphahat_conv, alphahat_div,
			  alphahat_chain, alphahat_other);
    
    strcpy(FN, "data/stats");
    strcat(FN, FNbase);
    strcat(FN, ".dat");
    fhnd = fopen(FN, "w");
    if(fhnd==NULL) {
      cerr << "Couldn't open outfile file " << FN << "\n";
      exit(-1);
    }
    fprintf(fhnd, "%e %e %e %e %e %e\n", phat, alphahat_recip, 
	    alphahat_conv, alphahat_div, alphahat_chain, alphahat_other);
    fclose(fhnd);
    
    
    cout << "done\n";
    cout << "\nActual statistics of matrix:\n";
    cout << "phat = " << phat << "\n";
    cout << "alphahats:\n";
    cout << "alpha_recip = " << alphahat_recip
	 << ", alpha_conv = " << alphahat_conv
	 << ", alpha_div = " << alphahat_div
	 << ", alpha_chain = " << alphahat_chain
	 << ", alpha_other = " << alphahat_other << "\n";
    
  }

  else if(N_pops==2) {
    double phat[2][2];
    double alphahat_recip[2][2];
    double alphahat_conv[2][2][2];
    double alphahat_div[2][2][2];
    double alphahat_chain[2][2][2];

    calc_phat_alphahat(W, N_nodes, phat, alphahat_recip, 
		       alphahat_conv, alphahat_div, alphahat_chain);

    cout << "done\n";

    strcpy(FN, "data/stats");
    strcat(FN, FNbase);
    strcat(FN, ".dat");
    fhnd = fopen(FN, "w");
    if(fhnd==NULL) {
      cerr << "Couldn't open outfile file " << FN << "\n";
      exit(-1);
    }

  
    fprintf(fhnd, "%i %i ", N_nodes[1], N_nodes[2]);
  
    cout << "Actual statistics of matrix:\n";
    cout << "phat = ";
    for(int i=0; i<2; i++)
      for(int j=0; j<2; j++) {
	cout << phat[i][j] << " ";
	fprintf(fhnd, "%e ", phat[i][j]);
      }
    cout << "\n";
    cout << "alphahat_recip = ";
    for(int i=0; i<2; i++)
      for(int j=i; j<2; j++) {
	cout << alphahat_recip[i][j] << " ";
	fprintf(fhnd, "%e ", alphahat_recip[i][j]);
      }
    cout << "\n";
    cout << "alphahat_conv = "; 
    for(int i=0; i<2; i++)
      for(int j=0; j<2; j++)
	for(int k=j; k<2; k++) {
	  cout << alphahat_conv[i][j][k] << " ";
	  fprintf(fhnd, "%e ", alphahat_conv[i][j][k]);
	}
    cout << "\n";
    cout << "alphahat_div = ";
    for(int i=0; i<2; i++)
      for(int j=i; j<2; j++)
	for(int k=0; k<2; k++) {
	  cout << alphahat_div[i][j][k] << " ";
	  fprintf(fhnd, "%e ", alphahat_div[i][j][k]);
	}
    cout << "\n";
    cout << "alphahat_chain = ";
    for(int i=0; i<2; i++)
      for(int j=0; j<2; j++)
	for(int k=0; k<2; k++) {
	  cout << alphahat_chain[i][j][k] << " ";
	  fprintf(fhnd, "%e ", alphahat_chain[i][j][k]);
	}
    cout << "\n";
    cout.flush();

    fclose(fhnd);

  }
  gsl_matrix_free(W);


  return 0;

}
