#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_cdf.h>
#include "calc_sqrtcov_gen.hpp"

using namespace std;

int calc_sqrtcov_given_rhos_large_N
(int N_pops, int *N_nodes, double *sigma, double *rho_recip, double *rho_conv, 
 double *rho_div, double *rho_chain, 
 double *sqrt_diag, double *sqrt_recip, double *sqrt_conv, 
 double *sqrt_div, double *sqrt_chain, 
 double *sqrt_diag_last, double *sqrt_recip_last, double *sqrt_conv_last, 
 double *sqrt_div_last, double *sqrt_chain_last, 
 double cov_mult=1);

int calc_sqrtcov_given_rhos_refine
(int N_pops, int *N_nodes, double *sigma, double *rho_recip, double *rho_conv, 
 double *rho_div, double *rho_chain, 
 double *sqrt_diag, double *sqrt_recip, double *sqrt_conv, 
 double *sqrt_div, double *sqrt_chain);

int calc_sqrtcov_given_rhos
(int N_pops, int *N_nodes, double *sigma, double *rho_recip, double *rho_conv, 
 double *rho_div, double *rho_chain, 
 double *sqrt_diag, double *sqrt_recip, double *sqrt_conv, 
 double *sqrt_div, double *sqrt_chain) {

  double *sqrt_diag_last=new double[N_pops*N_pops];
  double *sqrt_recip_last=new double[N_pops*N_pops];
  double *sqrt_conv_last=new double[N_pops*N_pops*N_pops];
  double *sqrt_div_last=new double[N_pops*N_pops*N_pops];
  double *sqrt_chain_last=new double[N_pops*N_pops*N_pops];

  // initialize all sqrt's to zero
  for(int i=0; i<N_pops; i++)
    for(int j=0; j<N_pops; j++) {
      sqrt_diag[i*N_pops+j]=sqrt_recip[i*N_pops+j]=0.0;
      sqrt_diag_last[i*N_pops+j]=sqrt_recip_last[i*N_pops+j]=0.0;
      for(int k=0; k<N_pops; k++) {
	sqrt_conv[(i*N_pops+j)*N_pops+k]=
	  sqrt_div[(i*N_pops+j)*N_pops+k]=
	  sqrt_chain[(i*N_pops+j)*N_pops+k]=0.0;
	sqrt_conv_last[(i*N_pops+j)*N_pops+k]=
	  sqrt_div_last[(i*N_pops+j)*N_pops+k]=
	  sqrt_chain_last[(i*N_pops+j)*N_pops+k]=0.0;
      }
    }
  
  
  int status;
  status= calc_sqrtcov_given_rhos_large_N
    (N_pops, N_nodes, sigma, rho_recip, rho_conv, rho_div, rho_chain,
     sqrt_diag, sqrt_recip, sqrt_conv, sqrt_div, sqrt_chain,
     sqrt_diag_last, sqrt_recip_last, 
     sqrt_conv_last, sqrt_div_last, sqrt_chain_last,
     0);
  
  if(status) {
    delete [] sqrt_diag_last;
    delete [] sqrt_recip_last;
    delete [] sqrt_conv_last;
    delete [] sqrt_div_last;
    delete [] sqrt_chain_last;
    return status;
  }

  cout << "Found large N square root\n";
  cout.flush();

  
  // status = calc_sqrtcov_given_rhos_refine
  //   (N_pops, N_nodes, sigma, rho_recip, rho_conv, rho_div, rho_chain,
  //    sqrt_diag, sqrt_recip, sqrt_conv, sqrt_div, sqrt_chain);
  
  for(int i=0; i<N_pops; i++)
    for(int j=0; j<N_pops; j++) {
      sqrt_diag_last[i*N_pops+j]=sqrt_diag[i*N_pops+j];
      sqrt_recip_last[i*N_pops+j]=sqrt_recip[i*N_pops+j];
      for(int k=0; k<N_pops; k++) {
	sqrt_conv_last[(i*N_pops+j)*N_pops+k]
	  =sqrt_conv[(i*N_pops+j)*N_pops+k];
	sqrt_div_last[(i*N_pops+j)*N_pops+k]
	  =sqrt_div[(i*N_pops+j)*N_pops+k];
	sqrt_chain_last[(i*N_pops+j)*N_pops+k]
	  =sqrt_chain[(i*N_pops+j)*N_pops+k];
	
      }
    }
  
  double d_cov_mult=1.0;
  double cov_mult=d_cov_mult;
  for(int i=0; i<2000; i++) {
    status= calc_sqrtcov_given_rhos_large_N
      (N_pops, N_nodes, sigma, rho_recip, rho_conv, rho_div, rho_chain,
       sqrt_diag, sqrt_recip, sqrt_conv, sqrt_div, sqrt_chain, 
       sqrt_diag_last, sqrt_recip_last, 
       sqrt_conv_last, sqrt_div_last, sqrt_chain_last,
       cov_mult);
    
    if(status) {
      // cov_mult /= 2.0;
      // d_cov_mult /= 2.0;
      if(status>0)
	cout << "Found " << status << " negative evals\n";
      else
	exit(-1);
      //cout << "Reduced d_cov_mult to " << d_cov_mult << "\n";
    }
    //else {
      double diff=0.0;
      double sum=0.0;
      // it doesn't hurt to calculate diff of non-existent entries
      // since they are all zero
      for(int i=0; i<N_pops; i++)
	for(int j=0; j<N_pops; j++) {
	  diff += gsl_pow_2(sqrt_diag_last[i*N_pops+j]-sqrt_diag[i*N_pops+j]);
	  sum += gsl_pow_2(sqrt_diag[i*N_pops+j]);
	  sqrt_diag_last[i*N_pops+j]=sqrt_diag[i*N_pops+j];
	  diff += gsl_pow_2(sqrt_recip_last[i*N_pops+j]-sqrt_recip[i*N_pops+j]);
	  sum += gsl_pow_2(sqrt_recip[i*N_pops+j]);
	  sqrt_recip_last[i*N_pops+j]=sqrt_recip[i*N_pops+j];
	  for(int k=0; k<N_pops; k++) {
	    diff += gsl_pow_2(sqrt_conv_last[(i*N_pops+j)*N_pops+k]
			      -sqrt_conv[(i*N_pops+j)*N_pops+k]);
	    sum += gsl_pow_2(sqrt_conv[(i*N_pops+j)*N_pops+k]);
	    sqrt_conv_last[(i*N_pops+j)*N_pops+k]
	      =sqrt_conv[(i*N_pops+j)*N_pops+k];
	    diff += gsl_pow_2(sqrt_div_last[(i*N_pops+j)*N_pops+k]
			      -sqrt_div[(i*N_pops+j)*N_pops+k]);
	    sum += gsl_pow_2(sqrt_div[(i*N_pops+j)*N_pops+k]);
	    sqrt_div_last[(i*N_pops+j)*N_pops+k]
	      =sqrt_div[(i*N_pops+j)*N_pops+k];
	    diff += gsl_pow_2(sqrt_chain_last[(i*N_pops+j)*N_pops+k]
			      -sqrt_chain[(i*N_pops+j)*N_pops+k]);
	    sum += gsl_pow_2(sqrt_chain[(i*N_pops+j)*N_pops+k]);
	    sqrt_chain_last[(i*N_pops+j)*N_pops+k]
	      =sqrt_chain[(i*N_pops+j)*N_pops+k];

	  }
	}

      cout << "diff = " << diff << ", sum = " << sum << ", ratio = " 
	   << diff/sum << "\n";
      
      if(cov_mult==1.0) {
	if(diff/sum < 1E-5)
	  break;
      }
      else { //if(diff/sum < 1E-5) {
	
	cov_mult += d_cov_mult;
	if(cov_mult >1.0)
	  cov_mult=1.0;
	cout << "cov_mult = " << cov_mult << "\n";
      }
      //}
  }
  // cout << "before refine\n";
  //   cout << "sqrt_diag = \n";
  //   for(int i=0; i<N_pops; i++) {
  //     for(int j=0; j<N_pops; j++)
  // 	cout << sqrt_diag[i*N_pops+j] << " ";
  //     cout << "\n";
  //   }
  //   cout << "\n";
  //   cout << "sqrt_recip = \n";
  //   for(int i=0; i<N_pops; i++) {
  //     for(int j=i; j<N_pops; j++)
  // 	cout << sqrt_recip[i*N_pops+j] << " ";
  //     cout << "\n";
  //   }
  //   cout << "\n";
  //   cout << "sqrt_conv = \n";
  //   for(int i=0; i<N_pops; i++) {
  //     for(int j=0; j<N_pops; j++) {
  // 	for(int k=j; k<N_pops; k++)
  // 	  cout << sqrt_conv[(i*N_pops+j)*N_pops+k] << " ";
  // 	cout << "\n";
  //     }
  //     cout << "\n";
  //   }
  //   cout << "sqrt_div = \n";
  //   for(int i=0; i<N_pops; i++) {
  //     for(int j=i; j<N_pops; j++) {
  // 	for(int k=0; k<N_pops; k++)
  // 	  cout << sqrt_div[(i*N_pops+j)*N_pops+k] << " ";
  // 	cout << "\n";
  //     }
  //     cout << "\n";
  //   }
  //   cout << "sqrt_chain = \n";
  //   for(int i=0; i<N_pops; i++) {
  //     for(int j=0; j<N_pops; j++) {
  // 	for(int k=0; k<N_pops; k++)
  // 	  cout << sqrt_chain[(i*N_pops+j)*N_pops+k] << " ";
  // 	cout << "\n";
  //     }
  //     cout << "\n";
  //   }


  // status = calc_sqrtcov_given_rhos_refine
  //   (N_pops, N_nodes, sigma, rho_recip, rho_conv, rho_div, rho_chain,
  //    sqrt_diag, sqrt_recip, sqrt_conv, sqrt_div, sqrt_chain);


  // double diff=0.0;
  // double sum=0.0;
  // // it doesn't hurt to calculate diff of non-existent entries
  // // since they are all zero
  // for(int i=0; i<N_pops; i++)
  //   for(int j=0; j<N_pops; j++) {
  //     diff += gsl_pow_2(sqrt_diag_last[i*N_pops+j]-sqrt_diag[i*N_pops+j]);
  //     sum += gsl_pow_2(sqrt_diag[i*N_pops+j]);
  //     sqrt_diag_last[i*N_pops+j]=sqrt_diag[i*N_pops+j];
  //     diff += gsl_pow_2(sqrt_recip_last[i*N_pops+j]-sqrt_recip[i*N_pops+j]);
  //     sum += gsl_pow_2(sqrt_recip[i*N_pops+j]);
  //     sqrt_recip_last[i*N_pops+j]=sqrt_recip[i*N_pops+j];
  //     for(int k=0; k<N_pops; k++) {
  // 	diff += gsl_pow_2(sqrt_conv_last[(i*N_pops+j)*N_pops+k]
  // 			  -sqrt_conv[(i*N_pops+j)*N_pops+k]);
  // 	sum += gsl_pow_2(sqrt_conv[(i*N_pops+j)*N_pops+k]);
  // 	sqrt_conv_last[(i*N_pops+j)*N_pops+k]
  // 	  =sqrt_conv[(i*N_pops+j)*N_pops+k];
  // 	diff += gsl_pow_2(sqrt_div_last[(i*N_pops+j)*N_pops+k]
  // 			  -sqrt_div[(i*N_pops+j)*N_pops+k]);
  // 	sum += gsl_pow_2(sqrt_div[(i*N_pops+j)*N_pops+k]);
  // 	sqrt_div_last[(i*N_pops+j)*N_pops+k]
  // 	  =sqrt_div[(i*N_pops+j)*N_pops+k];
  // 	diff += gsl_pow_2(sqrt_chain_last[(i*N_pops+j)*N_pops+k]
  // 			  -sqrt_chain[(i*N_pops+j)*N_pops+k]);
  // 	sum += gsl_pow_2(sqrt_chain[(i*N_pops+j)*N_pops+k]);
  // 	sqrt_chain_last[(i*N_pops+j)*N_pops+k]
  // 	  =sqrt_chain[(i*N_pops+j)*N_pops+k];
	
  //     }
  //   }
  
  // cout << "diff = " << diff << ", sum = " << sum << ", ratio = " 
  //      << diff/sum << "\n";
  


  // for(int i=0; i<10; i++) {
  //   status= calc_sqrtcov_given_rhos_large_N
  //     (N_pops, N_nodes, sigma, rho_recip, rho_conv, rho_div, rho_chain,
  //      sqrt_diag, sqrt_recip, sqrt_conv, sqrt_div, sqrt_chain,
  //      sqrt_diag_last, sqrt_recip_last, 
  //      sqrt_conv_last, sqrt_div_last, sqrt_chain_last);
    
  //   if(0) {//status) {
  //     delete [] sqrt_diag_last;
  //     delete [] sqrt_recip_last;
  //     delete [] sqrt_conv_last;
  //     delete [] sqrt_div_last;
  //     delete [] sqrt_chain_last;
  //     return status;
  //   }

  //   double diff=0.0;
  //   double sum=0.0;
  //   // it doesn't hurt to calculate diff of non-existent entries
  //   // since they are all zero
  //   for(int i=0; i<N_pops; i++)
  //     for(int j=0; j<N_pops; j++) {
  // 	diff += gsl_pow_2(sqrt_diag_last[i*N_pops+j]-sqrt_diag[i*N_pops+j]);
  // 	sum += gsl_pow_2(sqrt_diag[i*N_pops+j]);
  // 	sqrt_diag_last[i*N_pops+j]=sqrt_diag[i*N_pops+j];
  // 	diff += gsl_pow_2(sqrt_recip_last[i*N_pops+j]-sqrt_recip[i*N_pops+j]);
  // 	sum += gsl_pow_2(sqrt_recip[i*N_pops+j]);
  // 	sqrt_recip_last[i*N_pops+j]=sqrt_recip[i*N_pops+j];
  // 	for(int k=0; k<N_pops; k++) {
  // 	  diff += gsl_pow_2(sqrt_conv_last[(i*N_pops+j)*N_pops+k]
  // 			    -sqrt_conv[(i*N_pops+j)*N_pops+k]);
  // 	  sum += gsl_pow_2(sqrt_conv[(i*N_pops+j)*N_pops+k]);
  // 	  sqrt_conv_last[(i*N_pops+j)*N_pops+k]
  // 	    =sqrt_conv[(i*N_pops+j)*N_pops+k];
  // 	  diff += gsl_pow_2(sqrt_div_last[(i*N_pops+j)*N_pops+k]
  // 			    -sqrt_div[(i*N_pops+j)*N_pops+k]);
  // 	  sum += gsl_pow_2(sqrt_div[(i*N_pops+j)*N_pops+k]);
  // 	  sqrt_div_last[(i*N_pops+j)*N_pops+k]
  // 	    =sqrt_div[(i*N_pops+j)*N_pops+k];
  // 	  diff += gsl_pow_2(sqrt_chain_last[(i*N_pops+j)*N_pops+k]
  // 			    -sqrt_chain[(i*N_pops+j)*N_pops+k]);
  // 	  sum += gsl_pow_2(sqrt_chain[(i*N_pops+j)*N_pops+k]);
  // 	  sqrt_chain_last[(i*N_pops+j)*N_pops+k]
  // 	    =sqrt_chain[(i*N_pops+j)*N_pops+k];

  // 	}
  //     }

  //   cout << "diff = " << diff << ", sum = " << sum << ", ratio = " 
  //   	 << diff/sum << "\n";

  //   if(diff/sum < 1E-12)
  //     break;
  // }



  delete [] sqrt_diag_last;
  delete [] sqrt_recip_last;
  delete [] sqrt_conv_last;
  delete [] sqrt_div_last;
  delete [] sqrt_chain_last;

  return 0;//status;


}


// calculate the components of the sqrt of the covariance matrix
// assuming that the number of neurons N_nodes are large
int calc_sqrtcov_given_rhos_large_N
(int N_pops, int *N_nodes, double *sigma, double *rho_recip, double *rho_conv, 
 double *rho_div, double *rho_chain, 
 double *sqrt_diag, double *sqrt_recip, double *sqrt_conv, 
 double *sqrt_div, double *sqrt_chain,
 double *sqrt_diag_last, double *sqrt_recip_last, double *sqrt_conv_last, 
 double *sqrt_div_last, double *sqrt_chain_last, 
 double cov_mult) {
  
  int found_neg_eval=0;
  double largest_neg_eval=0.0;

  int N_nodes_tot=0;
  for(int i=0; i<N_pops; i++) 
    N_nodes_tot += N_nodes[i];
  
  double *N_nodes_sqrt = new double[N_pops];
  
  for(int i=0; i<N_pops; i++) 
    N_nodes_sqrt[i] = sqrt((double)N_nodes[i]);
  
  // for large N, the equations for sqrt_conv, sqrt_div, and sqrt_chain
  // decouple from the rest.  More over, they decouple into N_pops groups
  // of N_pops*(2*N_pops+1) equations based on the pop of 
  // the central neuron in the motif
  
  // moreover, these N_pops*(2*N_pops+1) equations can be written as 
  // solving for the square root of a 2*N_pops x 2*N_pops covariance matrix
  // Hence one can quickly determine if the equations have a real solution
  // and find that real solution
  
  const size_t nmat=2*N_pops;
  gsl_eigen_symmv_workspace *work_eig=gsl_eigen_symmv_alloc(nmat);
  
  gsl_matrix *A = gsl_matrix_alloc(nmat,nmat);    // the 4x4 covariance matrix
  gsl_matrix *sqrtA =gsl_matrix_alloc(nmat,nmat); // its square root
  gsl_vector *evals=gsl_vector_alloc(nmat);       // evals of A
  gsl_matrix *evecs=gsl_matrix_alloc(nmat,nmat);  // evects of A
  
  for(int pop1=0; pop1 <N_pops; pop1++) {
    // Need to scale the rho's appropriately so the equations
    // can be written as finding the square root of covariance matrix A
    for(int pop2=0; pop2<N_pops; pop2++) {
      int first_pop12, second_pop12;
      if(pop1 <= pop2) {
	first_pop12=pop1;
	second_pop12=pop2;
      }
      else {
	first_pop12=pop2;
	second_pop12=pop1;
      }
      for(int pop3=pop2; pop3<N_pops; pop3++ ) {
	int first_pop13, second_pop13;
	if(pop1 <= pop3) {
	  first_pop13=pop1;
	  second_pop13=pop3;
	}
	else {
	  first_pop13=pop3;
	  second_pop13=pop1;
	}
	
	double temp_conv = sigma[pop1*N_pops+pop2]*sigma[pop1*N_pops+pop3]
	  *rho_conv[(pop1*N_pops+pop2)*N_pops+pop3];

	// subtract off additional components if sqrt's are nonzero
	double finite_N_correct
	  = -(sqrt_diag_last[pop1*N_pops+pop2]+sqrt_diag_last[pop1*N_pops+pop3])
	  * sqrt_conv_last[(pop1*N_pops+pop2)*N_pops+pop3]
	  - sqrt_recip_last[first_pop12*N_pops+second_pop12]
	  * sqrt_chain_last[(pop2*N_pops+pop1)*N_pops+pop3]
	  - sqrt_recip_last[first_pop13*N_pops+second_pop13]
	  * sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop2]
	  - sqrt_div_last[(first_pop13*N_pops+second_pop13)*N_pops+pop2]
	  * sqrt_chain_last[(pop1*N_pops+pop3)*N_pops+pop2]
	  - sqrt_div_last[(first_pop12*N_pops+second_pop12)*N_pops+pop3]
	  * sqrt_chain_last[(pop1*N_pops+pop2)*N_pops+pop3];
	finite_N_correct 
	  +=sqrt_conv_last[(pop1*N_pops+first_pop12)*N_pops+second_pop12]
	  *sqrt_conv_last[(pop1*N_pops+first_pop13)*N_pops+second_pop13]
	  +sqrt_chain_last[(pop1*N_pops+pop1)*N_pops+pop2]
	  *sqrt_chain_last[(pop1*N_pops+pop1)*N_pops+pop3];
	finite_N_correct 
	  +=sqrt_conv_last[(pop1*N_pops+pop2)*N_pops+pop2]
	  *sqrt_conv_last[(pop1*N_pops+pop2)*N_pops+pop3]
	  +sqrt_chain_last[(pop2*N_pops+pop1)*N_pops+pop2]
	  *sqrt_chain_last[(pop2*N_pops+pop1)*N_pops+pop3];
	finite_N_correct 
	  +=sqrt_conv_last[(pop1*N_pops+pop2)*N_pops+pop3]
	  *sqrt_conv_last[(pop1*N_pops+pop3)*N_pops+pop3]
	  +sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop2]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop3];
	
	temp_conv += finite_N_correct*cov_mult;
	
	gsl_matrix_set(A,pop3,pop2, 
		       N_nodes_sqrt[pop2]*N_nodes_sqrt[pop3]*temp_conv);
	

	double temp_div =sigma[pop2*N_pops+pop1]*sigma[pop3*N_pops+pop1]
	  *rho_div[(pop2*N_pops+pop3)*N_pops+pop1];
	// subtract off additional components if sqrt's are nonzero
	finite_N_correct
	  =-(sqrt_diag_last[pop2*N_pops+pop1]+sqrt_diag_last[pop3*N_pops+pop1])
	  *sqrt_div_last[(pop2*N_pops+pop3)*N_pops+pop1]
	  -sqrt_recip_last[first_pop12*N_pops+second_pop12]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop2]
	  -sqrt_recip_last[first_pop13*N_pops+second_pop13]
	  *sqrt_chain_last[(pop2*N_pops+pop1)*N_pops+pop3]
	  -sqrt_conv_last[(pop2*N_pops+first_pop13)*N_pops+second_pop13]
	  *sqrt_chain_last[(pop2*N_pops+pop3)*N_pops+pop1]
	  -sqrt_conv_last[(pop3*N_pops+first_pop12)*N_pops+second_pop12]
	  *sqrt_chain_last[(pop3*N_pops+pop2)*N_pops+pop1];

	finite_N_correct
	  +=sqrt_div_last[(first_pop12*N_pops+second_pop12)*N_pops+pop1]
	  *sqrt_div_last[(first_pop13*N_pops+second_pop13)*N_pops+pop1]
	  +sqrt_chain_last[(pop2*N_pops+pop1)*N_pops+pop1]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop1];
	finite_N_correct
	  +=sqrt_div_last[(pop2*N_pops+pop2)*N_pops+pop1]
	  *sqrt_div_last[(pop2*N_pops+pop3)*N_pops+pop1]
	  +sqrt_chain_last[(pop2*N_pops+pop1)*N_pops+pop2]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop2];
	finite_N_correct
	  +=sqrt_div_last[(pop2*N_pops+pop3)*N_pops+pop1]
	  *sqrt_div_last[(pop3*N_pops+pop3)*N_pops+pop1]
	  +sqrt_chain_last[(pop2*N_pops+pop1)*N_pops+pop3]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop3];

	temp_div += finite_N_correct*cov_mult;
	gsl_matrix_set(A,pop3+N_pops,pop2+N_pops, 
		       N_nodes_sqrt[pop2]*N_nodes_sqrt[pop3]*temp_div);
      }
      for(int pop3=0; pop3<N_pops; pop3++ ) {
	int first_pop13, second_pop13, first_pop23, second_pop23;
	if(pop1 <= pop3) {
	  first_pop13=pop1;
	  second_pop13=pop3;
	}
	else {
	  first_pop13=pop3;
	  second_pop13=pop1;
	}
	if(pop2 <= pop3) {
	  first_pop23=pop2;
	  second_pop23=pop3;
	}
	else {
	  first_pop23=pop3;
	  second_pop23=pop2;
	}
	double temp_chain = sigma[pop3*N_pops+pop1]*sigma[pop1*N_pops+pop2]
	  *rho_chain[(pop3*N_pops+pop1)*N_pops+pop2];
	// subtract off additional components if sqrt's are nonzero
	double finite_N_correct
	  =- (sqrt_diag_last[pop3*N_pops+pop1]+sqrt_diag_last[pop1*N_pops+pop2])
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop2]
	  -sqrt_recip_last[first_pop13*N_pops+second_pop13]
	  *sqrt_conv_last[(pop1*N_pops+first_pop23)*N_pops+second_pop23]
	  -sqrt_recip_last[first_pop12*N_pops+second_pop12]
	  *sqrt_div_last[(first_pop23*N_pops+second_pop23)*N_pops+pop1]
	  -sqrt_conv_last[(pop3*N_pops+first_pop12)*N_pops+second_pop12]
	  *sqrt_div_last[(first_pop13*N_pops+second_pop13)*N_pops+pop2]
	  -sqrt_chain_last[(pop2*N_pops+pop3)*N_pops+pop1]
	  *sqrt_chain_last[(pop1*N_pops+pop2)*N_pops+pop3];
	// subtract off overcounting from when pop2 match pop3,pop1,pop2
	finite_N_correct
	  += sqrt_conv_last[(pop1*N_pops+first_pop23)*N_pops+second_pop23]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop3]
	  +sqrt_div_last[(pop3*N_pops+pop3)*N_pops+pop1]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop2];
	finite_N_correct
	  += sqrt_conv_last[(pop1*N_pops+first_pop12)*N_pops+second_pop12]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop1]
	  +sqrt_div_last[(first_pop13*N_pops+second_pop13)*N_pops+pop1]
	  *sqrt_chain_last[(pop1*N_pops+pop1)*N_pops+pop2];
	finite_N_correct
	  += sqrt_conv_last[(pop1*N_pops+pop2)*N_pops+pop2]
	  *sqrt_chain_last[(pop3*N_pops+pop1)*N_pops+pop2]
	  +sqrt_div_last[(first_pop23*N_pops+second_pop23)*N_pops+pop1]
	  *sqrt_chain_last[(pop2*N_pops+pop1)*N_pops+pop2];

	temp_chain += finite_N_correct*cov_mult;

	gsl_matrix_set(A,pop3+N_pops,pop2, 
		       N_nodes_sqrt[pop2]*N_nodes_sqrt[pop3]*temp_chain);
      }
    }
    
    // to calculate square root of A
    // 1. take it's eigen decomposition
    // 2. take the square root of its eigenvalues
    // 3. reconstruct with new eigenvalues to get a square root of A

    gsl_eigen_symmv(A, evals, evecs, work_eig);

    double max_eval=-1E100;
    double min_eval=1E100;
    for(size_t i=0; i<nmat; i++) {
      double the_eval = gsl_vector_get(evals,i);
      if(the_eval > max_eval)
	max_eval=the_eval;
      if(the_eval < min_eval)
	min_eval=the_eval;
      if(the_eval <= 0) {
	if(the_eval > -1E-7) {
	  // allow eigenvalues to be slightly negative due to
	  // roundoff error
	  the_eval=0;
	}
	else if(the_eval > -1E-0) {
	  found_neg_eval++;
	  if(the_eval < largest_neg_eval)
	    largest_neg_eval = the_eval;
	  //cout << "found_neg_eval = " << found_neg_eval << "\n";
	  the_eval=0;
	}
	else {
	  // if have a negative eigenvalue, can't take square root
	  // system of equations does not have a real solution
	  // (at least in limit of large N)
	  cout << "Found a negative eval(" << i <<")=" << the_eval 
	       << " for pop1=" << pop1 << "\n";
	  delete [] N_nodes_sqrt;
	  gsl_eigen_symmv_free(work_eig);
	  gsl_matrix_free(A);
	  gsl_matrix_free(sqrtA);
	  gsl_matrix_free(evecs);
	  gsl_vector_free(evals);
	  return -1;
	}
      }

      // scale eigenvector by fourth root of eval so 
      // reconstruction with be based on square root of eval
      gsl_vector_view col = gsl_matrix_column(evecs,i);
      gsl_vector_scale(&col.vector, sqrt(sqrt(the_eval)));
    }

    cout << "min_eval = " << min_eval
	 <<", max_eval = " << max_eval << "\n";
    // reconstruct to get sqrt A
    gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,evecs,evecs,0,sqrtA);
		 
    // undo scaling to get elements of the square root of 
    // original covariance matrix
    for(int i=0; i<N_pops; i++) {
      for(int j=i; j<N_pops; j++ ) {
	sqrt_conv[(pop1*N_pops + i)*N_pops+j]
	  = gsl_matrix_get(sqrtA,j,i)
	  /(N_nodes_sqrt[i]*N_nodes_sqrt[j]);
	sqrt_div[(i*N_pops + j)*N_pops+pop1]
	  = gsl_matrix_get(sqrtA,j+N_pops,i+N_pops)
	  /(N_nodes_sqrt[i]*N_nodes_sqrt[j]);
      }
      for(int j=0; j<N_pops; j++) {
	sqrt_chain[(j*N_pops+pop1)*N_pops+i]
	  = gsl_matrix_get(sqrtA,j+N_pops,i)
	  /(N_nodes_sqrt[i]*N_nodes_sqrt[j]);
      }
    }
    
    // can solve for sqrt_recip and sqrt_diag entries just involving pop1
    int ind_pop1_pop1 = pop1*N_pops+pop1;
    int ind_pop1_pop1_pop1 = ind_pop1_pop1*N_pops+pop1;
    double temp1 = gsl_pow_2(sigma[ind_pop1_pop1])
      - (N_nodes[pop1]-2)
      *(gsl_pow_2(sqrt_conv[ind_pop1_pop1_pop1])
	+ gsl_pow_2(sqrt_div[ind_pop1_pop1_pop1])
	+ 2*gsl_pow_2(sqrt_chain[ind_pop1_pop1_pop1]));
    double temp2 = gsl_pow_2(sigma[ind_pop1_pop1])*rho_recip[ind_pop1_pop1]
      - (N_nodes[pop1]-2)
      *2*(sqrt_conv[ind_pop1_pop1_pop1] + sqrt_div[ind_pop1_pop1_pop1])
      *sqrt_chain[ind_pop1_pop1_pop1];


    for(int i=0; i<N_pops;i++) {
      int pop_first, pop_second;  // need smallest pop first for conv,div
      if(i==pop1)
	continue;
      if(i<pop1) {
	pop_first=i;
	pop_second=pop1;
      }
      else {
	pop_first=pop1;
	pop_second=i;
      }

      int ind_pop1_f_s = (pop1*N_pops+pop_first)*N_pops+pop_second;
      int ind_f_s_pop1 = (pop_first*N_pops+pop_second)*N_pops+pop1;
      int ind_pop1_pop1_i = (pop1*N_pops+pop1)*N_pops+i;
      int ind_i_pop1_pop1 = (i*N_pops+pop1)*N_pops+pop1;
      temp1 -=
	N_nodes[i]*(gsl_pow_2(sqrt_conv[ind_pop1_f_s])
		    + gsl_pow_2(sqrt_div[ind_f_s_pop1])
		    + gsl_pow_2(sqrt_chain[ind_pop1_pop1_i])
		    + gsl_pow_2(sqrt_chain[ind_i_pop1_pop1]));
      temp2 -=
	N_nodes[i]*2*(sqrt_conv[ind_pop1_f_s]
		      *sqrt_chain[ind_pop1_pop1_i]
		      +sqrt_div[ind_f_s_pop1]
		      *sqrt_chain[ind_i_pop1_pop1]);
    }
    // if both temp1 and temp2 are effectively zero, set 
    // sqrt_diag and sqrt_recip to zero
    if(fabs(temp1) < 1E-15
       && fabs(temp2) < 1E-15) {
      sqrt_diag[ind_pop1_pop1]=0.0;
      sqrt_recip[ind_pop1_pop1] = 0.0;
    }
    else if(fabs(temp1) >= fabs(temp2)) {
      sqrt_diag[ind_pop1_pop1] = sqrt((temp1+sqrt(temp1*temp1-temp2*temp2))
					   /2.0);
      sqrt_recip[ind_pop1_pop1] = temp2/(2.0*sqrt_diag[ind_pop1_pop1]);
    }
    else {
      cout << "temp1 = " << temp1
	   << ", temp2 = " << temp2 << "\n";
      // if can't get real solution to sqrt_diag, original system did
      // not have a real solution (at least for large N)
      cout << "Can't calculate sqrt_diag[" << ind_pop1_pop1 
	   << "]\n";
      delete [] N_nodes_sqrt;
      gsl_eigen_symmv_free(work_eig);
      gsl_matrix_free(A);
      gsl_matrix_free(sqrtA);
      gsl_matrix_free(evecs);
      gsl_vector_free(evals);
      return -1;
    }

  }

  delete N_nodes_sqrt;
  gsl_eigen_symmv_free(work_eig);
  gsl_matrix_free(A);
  gsl_matrix_free(sqrtA);
  gsl_matrix_free(evecs);
  gsl_vector_free(evals);
  

  // lastly calculate sqrt_diag and sqrt_recip for terms involving
  // two different neuron populations
  for(int pop1=0; pop1<N_pops; pop1++) 
    for(int pop2=pop1+1; pop2<N_pops; pop2++) {
      int ind_1_2 = pop1*N_pops+pop2;
      int ind_2_1 = pop2*N_pops+pop1;
      int ind_1_1_2 = (pop1*N_pops+pop1)*N_pops+pop2;
      int ind_1_2_1 = ind_1_2*N_pops+pop1;
      int ind_1_2_2 = ind_1_2*N_pops+pop2;
      int ind_2_2_1 = (pop2*N_pops+pop2)*N_pops+pop1;
      int ind_2_1_1 = ind_2_1*N_pops+pop1;
      int ind_2_1_2 = ind_2_1*N_pops+pop2;
      
      double temp12 = gsl_pow_2(sigma[ind_1_2])
	- (N_nodes[pop1]-1)*(gsl_pow_2(sqrt_conv[ind_1_1_2])
			     + gsl_pow_2(sqrt_div[ind_1_1_2])
			     + gsl_pow_2(sqrt_chain[ind_1_2_1])
			     + gsl_pow_2(sqrt_chain[ind_1_1_2]))
	- (N_nodes[pop2]-1)*(gsl_pow_2(sqrt_conv[ind_1_2_2])
			     + gsl_pow_2(sqrt_div[ind_1_2_2])
			     + gsl_pow_2(sqrt_chain[ind_1_2_2])
			     + gsl_pow_2(sqrt_chain[ind_2_1_2]));
      double temp21 = gsl_pow_2(sigma[ind_2_1])
	- (N_nodes[pop1]-1)*(gsl_pow_2(sqrt_conv[ind_2_1_1])
			     + gsl_pow_2(sqrt_div[ind_1_2_1])
			     + gsl_pow_2(sqrt_chain[ind_2_1_1])
			     + gsl_pow_2(sqrt_chain[ind_1_2_1]))
	- (N_nodes[pop2]-1)*(gsl_pow_2(sqrt_conv[ind_2_1_2])
			     + gsl_pow_2(sqrt_div[ind_2_2_1])
			     + gsl_pow_2(sqrt_chain[ind_2_1_2])
			     + gsl_pow_2(sqrt_chain[ind_2_2_1]));
      double temp3 = sigma[ind_1_2]*sigma[ind_2_1]*rho_recip[ind_1_2]
	- (N_nodes[pop1]-1)
	*(sqrt_conv[ind_1_1_2]*sqrt_chain[ind_2_1_1] 
	  + sqrt_conv[ind_2_1_1]*sqrt_chain[ind_1_2_1]
	  + sqrt_div[ind_1_1_2]*sqrt_chain[ind_1_2_1] 
	  + sqrt_div[ind_1_2_1]*sqrt_chain[ind_1_1_2])
	- (N_nodes[pop2]-1)
	*(sqrt_conv[ind_1_2_2]*sqrt_chain[ind_2_1_2] 
	  + sqrt_conv[ind_2_1_2]*sqrt_chain[ind_1_2_2]
	  + sqrt_div[ind_1_2_2]*sqrt_chain[ind_2_2_1] 
	  + sqrt_div[ind_2_2_1]*sqrt_chain[ind_2_1_2]);
      
      for(int pop3=0; pop3 <N_pops; pop3++) {
	if(pop3 == pop1 || pop3 == pop2)
	  continue;
	
	// need smallest pop first for conv,div
	int first_pop23, second_pop23, first_pop13, second_pop13;
	if(pop3 < pop1) {
	  // pop3 < pop1 < pop2
	  first_pop23 = pop3;
	  second_pop23 = pop2;
	  first_pop13 = pop3;
	  second_pop13 = pop1;
	}
	else if(pop3 < pop2) {
	  // pop1 < pop3 < pop2
	  first_pop23 = pop3;
	  second_pop23 = pop2;
	  first_pop13 = pop1;
	  second_pop13 = pop3;
	}
	else {
	  // pop1 < pop2 < pop3
	  first_pop23 = pop2;
	  second_pop23 = pop3;
	  first_pop13 = pop1;
	  second_pop13 = pop3;
	}
      
	temp12 -= N_nodes[pop3]
	  *(gsl_pow_2(sqrt_conv[(pop1*N_pops+first_pop23)*N_pops+second_pop23])
	    +gsl_pow_2(sqrt_div[(first_pop13*N_pops+second_pop13)*N_pops+pop2])
	    +gsl_pow_2(sqrt_chain[(pop1*N_pops+pop2)*N_pops+pop3])
	    +gsl_pow_2(sqrt_chain[(pop3*N_pops+pop1)*N_pops+pop2]));
	
	temp21 -= N_nodes[pop3]
	  *(gsl_pow_2(sqrt_conv[(pop2*N_pops+first_pop13)*N_pops+second_pop13])
	    +gsl_pow_2(sqrt_div[(first_pop23*N_pops+second_pop23)*N_pops+pop1])
	    +gsl_pow_2(sqrt_chain[(pop2*N_pops+pop1)*N_pops+pop3])
	    +gsl_pow_2(sqrt_chain[(pop3*N_pops+pop2)*N_pops+pop1]));

	temp3 -= N_nodes[pop3]
	  *(sqrt_conv[(pop1*N_pops+first_pop23)*N_pops+second_pop23]
	    *sqrt_chain[(pop2*N_pops+pop1)*N_pops+pop3]
	    +sqrt_conv[(pop2*N_pops+first_pop13)*N_pops+second_pop13]
	    *sqrt_chain[(pop1*N_pops+pop2)*N_pops+pop3]
	    +sqrt_div[(first_pop13*N_pops+second_pop13)*N_pops+pop2]
	    *sqrt_chain[(pop3*N_pops+pop2)*N_pops+pop1]
	    +sqrt_div[(first_pop23*N_pops+second_pop23)*N_pops+pop1]
	    *sqrt_chain[(pop3*N_pops+pop1)*N_pops+pop2]);
      }

  
      // Address cases where temp12, temp21, and/or temp3 are effectively zero
      // (caused by a zero sigma)
      if(fabs(temp12) < 1E-15) {
	if(fabs(temp21) < 1E-15) {
	  if(fabs(temp3) < 1E-15) {
	    sqrt_diag[ind_1_2] = sqrt_diag[ind_2_1] = sqrt_recip[ind_1_2] = 0.0;
	  }
	  else {
	    cout << "Couldn't calculate sqrt_diag[" << ind_2_1 
		 << "], sqrt_diag[" << ind_1_1_2 << "] and sqrt_recip["
		 << ind_1_2 << "]\n";
	    return -1;
	  }
	}
	else {
	  // temp12 effectively zero but not temp21
	  if(fabs(temp3) < 1E-15) {
	    sqrt_diag[ind_1_2] = 0.0;
	    if(temp21 >= 0)
	      sqrt_diag[ind_2_1] = sqrt(temp21);
	    else {
	      cout << "Couldn't calculate sqrt_diag[" << ind_2_1 
		   << "]\n";
	      return -1;
	    }
	    sqrt_recip[ind_1_2]=0.0;
	  }
	  else {
	    cout << "Couldn't calculate sqrt_diag[" << ind_2_1 
		 << "], sqrt_diag[" << ind_1_1_2 << "] and sqrt_recip["
		 << ind_1_2 << "]\n";
	    return -1;
	  }
	}
      }
      else if(fabs(temp21) < 1E-15) {
	// temp21 effectively zero but not temp12
	if(fabs(temp3) < 1E-15) {
	  sqrt_diag[ind_2_1] = 0.0;
	  if(temp12 >= 0)
	    sqrt_diag[ind_1_2] = sqrt(temp12);
	  else {
	    cout << "Couldn't calculate sqrt_diag[" << ind_1_2 
		 << "]\n";
	    return -1;
	    }
	  sqrt_recip[ind_1_2]=0.0;
	}
	else {
	  cout << "Couldn't calculate sqrt_diag[" << ind_2_1 
	       << "], sqrt_diag[" << ind_1_1_2 << "] and sqrt_recip["
	       << ind_1_2 << "]\n";
	  return -1;
	}
      }
      else {
	// neither temp12 or temp21 effectively zero
	if(fabs(temp3) < 1E-15) {
	  sqrt_recip[ind_1_2] = 0.0;
	  sqrt_diag[ind_1_2] = sqrt(temp12);
	  sqrt_diag[ind_2_1] = sqrt(temp21);
	}
	else {
	
	  double temp4=temp12-temp21;
	  double temp32=temp3*temp3;
      
	  double tempa = temp4*temp4+4*temp32;
	  double tempb = -4*temp12*temp32 - 2*temp32*temp4 - 2*temp12*temp4*temp4;
	  double tempc = gsl_pow_2(temp32 + temp4*temp12);

	  sqrt_diag[ind_1_2] = sqrt((-tempb+sqrt(tempb*tempb-4*tempa*tempc))/(2*tempa));
	  if(!(sqrt_diag[ind_1_2] >= 0)) {
	    cout << "Couldn't calculate sqrt_diag[" << ind_1_2 
		 << "] = " << sqrt_diag[ind_1_2] <<"\n";
	    return -1;
	  }
	  // find sign in front of temp3 to use for sqrt_diag[ind_1_2]
	  // reuse temp4
	  temp4 = temp3*temp3/(temp12-sqrt_diag[ind_1_2]*sqrt_diag[ind_1_2]);
	  if(temp4 < 0) {
	    cout << "Couldn't calculate sqrt_diag[" << ind_2_1
		 << "]= " << sqrt_diag[ind_2_1] <<"\n";
	    return -1;
	  }
	  double thesign = GSL_SIGN((temp4+temp12-temp21)
				    /(2*sqrt_diag[ind_1_2]*sqrt(temp4)));
      
	  sqrt_diag[ind_2_1] = thesign*sqrt(temp4) - sqrt_diag[ind_1_2];
      
	  sqrt_recip[ind_1_2] = temp3/(sqrt_diag[ind_1_2]+sqrt_diag[ind_2_1]);
	}      
      }
    }
  

  if(found_neg_eval)
    cout << "Found " << found_neg_eval << ", largest_neg = "
	 << largest_neg_eval << "\n";
  return found_neg_eval;

}

struct pop_params
{
  double *sigma;
  double *rho_recip;
  double *rho_conv;
  double *rho_div;
  double *rho_chain;
  int N_pops;
  int *N_nodes;
};


int sqrtcov_f(const gsl_vector * x, void *params,
	    gsl_vector * f) {
  
  double *sigma= ((struct pop_params *) params)->sigma;
  double *rho_recip= ((struct pop_params *) params)->rho_recip;
  double *rho_conv= ((struct pop_params *) params)->rho_conv;
  double *rho_div= ((struct pop_params *) params)->rho_div;
  double *rho_chain= ((struct pop_params *) params)->rho_chain;
  int N_pops= ((struct pop_params *) params)->N_pops;
  int *N_nodes= ((struct pop_params *) params)->N_nodes;

  double *a=new double[N_pops*N_pops];
  double *b=new double[N_pops*N_pops];
  double *c=new double[N_pops*N_pops*N_pops];
  double *d=new double[N_pops*N_pops*N_pops];
  double *e=new double[N_pops*N_pops*N_pops];

  int indcount=0;

  // N_pops^2 a's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++) {
      if(sigma[i*N_pops+j]) {
	a[i*N_pops+j] = gsl_vector_get(x,indcount);
	indcount++;
      }
      else {
	a[i*N_pops+j] = 0;
      }
    }

  // N_pops*(N_pops+1)/2 b's
  for(int i=0; i<N_pops; i++) 
    for(int j=i; j<N_pops; j++) {
      if(sigma[i*N_pops+j]*sigma[j*N_pops+i]) {
	b[i*N_pops+j] = gsl_vector_get(x,indcount);
	indcount++;
      }
      else {
	b[i*N_pops+j] = 0;
      }
    }
  
  // N_pops^2*(N_pops+1)/2  c's 
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++)
      for(int k=j; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[i*N_pops+k]) {
	  c[(i*N_pops+j)*N_pops+k] = gsl_vector_get(x,indcount);
	  indcount++;
	}
	else {
	  c[(i*N_pops+j)*N_pops+k] = 0;
	}
      }

  // N_pops^2*(N_pops+1)/2 d's 
  for(int i=0; i<N_pops; i++) 
    for(int j=i; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+k]*sigma[j*N_pops+k]) {
	  d[(i*N_pops+j)*N_pops+k] = gsl_vector_get(x,indcount);
	  indcount++;
	}
	else {
	  d[(i*N_pops+j)*N_pops+k] = 0;
	}
      }

  // N_pops^3 e's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[j*N_pops+k]) {
	  e[(i*N_pops+j)*N_pops+k] = gsl_vector_get(x,indcount);
	  indcount++;
	}
	else {
	  e[(i*N_pops+j)*N_pops+k] = 0;
	}
      }
  
  double *f_diag=new double[N_pops*N_pops];
  double *f_recip=new double[N_pops*N_pops];
  double *f_conv=new double[N_pops*N_pops*N_pops];
  double *f_div=new double[N_pops*N_pops*N_pops];
  double *f_chain=new double[N_pops*N_pops*N_pops];


  for(int pop1=0; pop1<N_pops; pop1++) {
    f_diag[pop1*N_pops+pop1] = gsl_pow_2(a[pop1*N_pops+pop1]) 
      + gsl_pow_2(b[pop1*N_pops+pop1]) 
      + (N_nodes[pop1]-2)*(gsl_pow_2(c[(pop1*N_pops+pop1)*N_pops+pop1])
			   +gsl_pow_2(d[(pop1*N_pops+pop1)*N_pops+pop1]) 
			   + 2*gsl_pow_2(e[(pop1*N_pops+pop1)*N_pops+pop1]))
      - gsl_pow_2(sigma[pop1*N_pops+pop1]);
    
    f_recip[pop1*N_pops+pop1] = 2*a[pop1*N_pops+pop1]*b[pop1*N_pops+pop1]
      +(N_nodes[pop1]-2)*2*(c[(pop1*N_pops+pop1)*N_pops+pop1]
			    +d[(pop1*N_pops+pop1)*N_pops+pop1])
      *e[(pop1*N_pops+pop1)*N_pops+pop1]
      -gsl_pow_2(sigma[pop1*N_pops+pop1])*rho_recip[pop1*N_pops+pop1];
    
    
    for(int pop2=0; pop2<N_pops; pop2++) {
      int first_pop12, second_pop12;
      if(pop1 == pop2)
	continue;
      if(pop1 < pop2) {
	first_pop12=pop1;
	second_pop12=pop2;
      }
      else {
	first_pop12=pop2;
	second_pop12=pop1;
      }

      f_diag[pop1*N_pops+pop1] 
	+= N_nodes[pop2]
	*(gsl_pow_2(c[(pop1*N_pops+first_pop12)*N_pops+second_pop12])
	  + gsl_pow_2(d[(first_pop12*N_pops+second_pop12)*N_pops+pop1]) 
	  + gsl_pow_2(e[(pop1*N_pops+pop1)*N_pops+pop2]) 
	  + gsl_pow_2(e[(pop2*N_pops+pop1)*N_pops+pop1]));
    
      f_recip[pop1*N_pops+pop1] 
	+=N_nodes[pop2]*2
	*(c[(pop1*N_pops+first_pop12)*N_pops+second_pop12]
	  *e[(pop1*N_pops+pop1)*N_pops+pop2] 
	  +d[(first_pop12*N_pops+second_pop12)*N_pops+pop1]
	  *e[(pop2*N_pops+pop1)*N_pops+pop1]);
      

      f_diag[pop1*N_pops+pop2] = gsl_pow_2(a[pop1*N_pops+pop2]) 
	+ gsl_pow_2(b[first_pop12*N_pops+second_pop12]) 
	+(N_nodes[pop1]-1)
	*(gsl_pow_2(c[(pop1*N_pops+first_pop12)*N_pops+second_pop12])
	  +gsl_pow_2(d[(pop1*N_pops+pop1)*N_pops+pop2]) 
	  +gsl_pow_2(e[(pop1*N_pops+pop2)*N_pops+pop1]) 
	  +gsl_pow_2(e[(pop1*N_pops+pop1)*N_pops+pop2]))
	+(N_nodes[pop2]-1)
	*(gsl_pow_2(c[(pop1*N_pops+pop2)*N_pops+pop2])
	  +gsl_pow_2(d[(first_pop12*N_pops+second_pop12)*N_pops+pop2])
	  + gsl_pow_2(e[(pop1*N_pops+pop2)*N_pops+pop2])
	  + gsl_pow_2(e[(pop2*N_pops+pop1)*N_pops+pop2]))
	-gsl_pow_2(sigma[pop1*N_pops+pop2]);
      
      for(int pop3=0; pop3<N_pops; pop3++) {
	if(pop3==pop1 || pop3==pop2)
	  continue;
	
	// need smallest pop first for conv,div
	int first_pop23, second_pop23, first_pop13, second_pop13;
	if(pop3 < pop1) {
	  first_pop13 = pop3;
	  second_pop13 = pop1;
	}
	else {
	  first_pop13 = pop1;
	  second_pop13 = pop3;
	}
	if(pop3 < pop2) {
	  first_pop23 = pop3;
	  second_pop23 = pop2;
	}
	else {
	  first_pop23 = pop2;
	  second_pop23 = pop3;
	}
 

	f_diag[pop1*N_pops+pop2]
	  += N_nodes[pop3]
	  *(gsl_pow_2(c[(pop1*N_pops+first_pop23)*N_pops+second_pop23])
	    +gsl_pow_2(d[(first_pop13*N_pops+second_pop13)*N_pops+pop2])
	    +gsl_pow_2(e[(pop1*N_pops+pop2)*N_pops+pop3])
	    +gsl_pow_2(e[(pop3*N_pops+pop1)*N_pops+pop2]));

      }
    }
    for(int pop2=pop1+1; pop2<N_pops; pop2++) {
      
      f_recip[pop1*N_pops+pop2] = (a[pop1*N_pops+pop2]+a[pop2*N_pops+pop1])*b[pop1*N_pops+pop2]
	+ (N_nodes[pop1]-1)
	*(c[(pop1*N_pops+pop1)*N_pops+pop2]*e[(pop2*N_pops+pop1)*N_pops+pop1] 
	  + c[(pop2*N_pops+pop1)*N_pops+pop1]*e[(pop1*N_pops+pop2)*N_pops+pop1]
	  + d[(pop1*N_pops+pop1)*N_pops+pop2]*e[(pop1*N_pops+pop2)*N_pops+pop1]
	  + d[(pop1*N_pops+pop2)*N_pops+pop1]*e[(pop1*N_pops+pop1)*N_pops+pop2])
	+ (N_nodes[pop2]-1)
	*(c[(pop1*N_pops+pop2)*N_pops+pop2]*e[(pop2*N_pops+pop1)*N_pops+pop2] 
	  + c[(pop2*N_pops+pop1)*N_pops+pop2]*e[(pop1*N_pops+pop2)*N_pops+pop2]
	  + d[(pop1*N_pops+pop2)*N_pops+pop2]*e[(pop2*N_pops+pop2)*N_pops+pop1] 
	  + d[(pop2*N_pops+pop2)*N_pops+pop1]*e[(pop2*N_pops+pop1)*N_pops+pop2])
	-sigma[pop1*N_pops+pop2]*sigma[pop2*N_pops+pop1]*rho_recip[pop1*N_pops+pop2];
      
      
      for(int pop3=0; pop3<N_pops; pop3++) {
	if(pop3==pop1 || pop3==pop2)
	  continue;
	
	// need smallest pop first for conv,div
	int first_pop23, second_pop23, first_pop13, second_pop13;
	if(pop3 < pop1) {
	  first_pop13 = pop3;
	  second_pop13 = pop1;
	}
	else {
	  first_pop13 = pop1;
	  second_pop13 = pop3;
	}
	if(pop3 < pop2) {
	  first_pop23 = pop3;
	  second_pop23 = pop2;
	}
	else {
	  first_pop23 = pop2;
	  second_pop23 = pop3;
	}
	f_recip[pop1*N_pops+pop2] 
	  += N_nodes[pop3]*(c[(pop1*N_pops+first_pop23)*N_pops+second_pop23]
			    *e[(pop2*N_pops+pop1)*N_pops+pop3]
			    +c[(pop2*N_pops+first_pop13)*N_pops+second_pop13]
			    *e[(pop1*N_pops+pop2)*N_pops+pop3]
			    +d[(first_pop13*N_pops+second_pop13)*N_pops+pop2]
			    *e[(pop3*N_pops+pop2)*N_pops+pop1]
			    +d[(first_pop23*N_pops+second_pop23)*N_pops+pop1]
			    *e[(pop3*N_pops+pop1)*N_pops+pop2]);
      }
    }
  }
  for(int pop1=0; pop1<N_pops; pop1++) {
    for(int pop2=0; pop2<N_pops; pop2++) { 
      int first_pop12, second_pop12;
      if(pop1 <= pop2) {
	first_pop12=pop1;
	second_pop12=pop2;
      }
      else {
	first_pop12=pop2;
	second_pop12=pop1;
      }
      for(int pop3=pop2; pop3<N_pops; pop3++) {
	int first_pop13, second_pop13;
	if(pop1 <= pop3) {
	  first_pop13=pop1;
	  second_pop13=pop3;
	}
	else {
	  first_pop13=pop3;
	  second_pop13=pop1;
	}
	
	f_conv[(pop1*N_pops+pop2)*N_pops+pop3] 
	  = (a[pop1*N_pops+pop2]+a[pop1*N_pops+pop3])*c[(pop1*N_pops+pop2)*N_pops+pop3]
	  + b[first_pop12*N_pops+second_pop12]*e[(pop2*N_pops+pop1)*N_pops+pop3]
	  + b[first_pop13*N_pops+second_pop13]*e[(pop3*N_pops+pop1)*N_pops+pop2]
	  + d[(first_pop13*N_pops+second_pop13)*N_pops+pop2]
	  *e[(pop1*N_pops+pop3)*N_pops+pop2]
	  + d[(first_pop12*N_pops+second_pop12)*N_pops+pop3]
	  *e[(pop1*N_pops+pop2)*N_pops+pop3]
	  - sigma[pop1*N_pops+pop2]*sigma[pop1*N_pops+pop3]
	  *rho_conv[(pop1*N_pops+pop2)*N_pops+pop3];
	
	f_div[(pop2*N_pops+pop3)*N_pops+pop1]
	  =(a[pop2*N_pops+pop1]+a[pop3*N_pops+pop1])
	  *d[(pop2*N_pops+pop3)*N_pops+pop1]
	  +b[first_pop12*N_pops+second_pop12]
	  *e[(pop3*N_pops+pop1)*N_pops+pop2]
	  +b[first_pop13*N_pops+second_pop13]
	  *e[(pop2*N_pops+pop1)*N_pops+pop3]
	  +c[(pop2*N_pops+first_pop13)*N_pops+second_pop13]
	  *e[(pop2*N_pops+pop3)*N_pops+pop1]
	  +c[(pop3*N_pops+first_pop12)*N_pops+second_pop12]
	  *e[(pop3*N_pops+pop2)*N_pops+pop1]
	  -sigma[pop2*N_pops+pop1]*sigma[pop3*N_pops+pop1]
	  *rho_div[(pop2*N_pops+pop3)*N_pops+pop1];
	

	for(int pop4=0; pop4<N_pops; pop4++) {
	  int first_pop24, second_pop24, first_pop34, second_pop34;
	  if(pop2 <= pop4) {
	    first_pop24=pop2;
	    second_pop24=pop4;
	  }
	  else {
	    first_pop24=pop4;
	    second_pop24=pop2;
	  }
	  if(pop3 <= pop4) {
	    first_pop34=pop3;
	    second_pop34=pop4;
	  }
	  else {
	    first_pop34=pop4;
	    second_pop34=pop3;
	  }

	  f_conv[(pop1*N_pops+pop2)*N_pops+pop3] 
	    +=N_nodes[pop4]*(c[(pop1*N_pops+first_pop24)*N_pops+second_pop24]
			     *c[(pop1*N_pops+first_pop34)*N_pops+second_pop34]
			     +e[(pop4*N_pops+pop1)*N_pops+pop2]
			     *e[(pop4*N_pops+pop1)*N_pops+pop3]);
	  f_div[(pop2*N_pops+pop3)*N_pops+pop1]
	    +=N_nodes[pop4]*(d[(first_pop24*N_pops+second_pop24)*N_pops+pop1]
			     *d[(first_pop34*N_pops+second_pop34)*N_pops+pop1]
			     +e[(pop2*N_pops+pop1)*N_pops+pop4]
			     *e[(pop3*N_pops+pop1)*N_pops+pop4]);

	}
	// subtract off overcounting from when pop4 match pop1,pop2,pop3
	f_conv[(pop1*N_pops+pop2)*N_pops+pop3] 
	  -=c[(pop1*N_pops+first_pop12)*N_pops+second_pop12]
	  *c[(pop1*N_pops+first_pop13)*N_pops+second_pop13]
	  +e[(pop1*N_pops+pop1)*N_pops+pop2]*e[(pop1*N_pops+pop1)*N_pops+pop3];
	f_conv[(pop1*N_pops+pop2)*N_pops+pop3] 
	  -=c[(pop1*N_pops+pop2)*N_pops+pop2]
	  *c[(pop1*N_pops+pop2)*N_pops+pop3]
	  +e[(pop2*N_pops+pop1)*N_pops+pop2]*e[(pop2*N_pops+pop1)*N_pops+pop3];
	f_conv[(pop1*N_pops+pop2)*N_pops+pop3] 
	  -=c[(pop1*N_pops+pop2)*N_pops+pop3]
	  *c[(pop1*N_pops+pop3)*N_pops+pop3]
	  +e[(pop3*N_pops+pop1)*N_pops+pop2]*e[(pop3*N_pops+pop1)*N_pops+pop3];
	f_div[(pop2*N_pops+pop3)*N_pops+pop1]
	  -=d[(first_pop12*N_pops+second_pop12)*N_pops+pop1]
	  *d[(first_pop13*N_pops+second_pop13)*N_pops+pop1]
	  +e[(pop2*N_pops+pop1)*N_pops+pop1]*e[(pop3*N_pops+pop1)*N_pops+pop1];
	f_div[(pop2*N_pops+pop3)*N_pops+pop1]
	  -=d[(pop2*N_pops+pop2)*N_pops+pop1]
	  *d[(pop2*N_pops+pop3)*N_pops+pop1]
	  +e[(pop2*N_pops+pop1)*N_pops+pop2]*e[(pop3*N_pops+pop1)*N_pops+pop2];
	f_div[(pop2*N_pops+pop3)*N_pops+pop1]
	  -=d[(pop2*N_pops+pop3)*N_pops+pop1]
	  *d[(pop3*N_pops+pop3)*N_pops+pop1]
	  +e[(pop2*N_pops+pop1)*N_pops+pop3]*e[(pop3*N_pops+pop1)*N_pops+pop3];
	
      }
      
      for(int pop3=0; pop3<N_pops; pop3++) {
	int first_pop13, second_pop13, first_pop23, second_pop23;
	if(pop1 <= pop3) {
	  first_pop13=pop1;
	  second_pop13=pop3;
	}
	else {
	  first_pop13=pop3;
	  second_pop13=pop1;
	}
	if(pop2 <= pop3) {
	  first_pop23=pop2;
	  second_pop23=pop3;
	}
	else {
	  first_pop23=pop3;
	  second_pop23=pop2;
	}
	
	f_chain[(pop1*N_pops+pop2)*N_pops+pop3]
	  = (a[pop1*N_pops+pop2]+a[pop2*N_pops+pop3])
	  *e[(pop1*N_pops+pop2)*N_pops+pop3]
	  +b[first_pop12*N_pops+second_pop12]
	  *c[(pop2*N_pops+first_pop13)*N_pops+second_pop13]
	  +b[first_pop23*N_pops+second_pop23]
	  *d[(first_pop13*N_pops+second_pop13)*N_pops+pop2]
	  +c[(pop1*N_pops+first_pop23)*N_pops+second_pop23]
	  *d[(first_pop12*N_pops+second_pop12)*N_pops+pop3]
	  +e[(pop3*N_pops+pop1)*N_pops+pop2]
	  *e[(pop2*N_pops+pop3)*N_pops+pop1]
	  -sigma[pop1*N_pops+pop2]*sigma[pop2*N_pops+pop3]
	  *rho_chain[(pop1*N_pops+pop2)*N_pops+pop3];
	
	for(int pop4=0; pop4<N_pops; pop4++) {
	  int first_pop14, second_pop14, first_pop34, second_pop34;
	  if(pop1 <= pop4) {
	    first_pop14=pop1;
	    second_pop14=pop4;
	  }
	  else {
	    first_pop14=pop4;
	    second_pop14=pop1;
	  }
	  if(pop3 <= pop4) {
	    first_pop34=pop3;
	    second_pop34=pop4;
	  }
	  else {
	    first_pop34=pop4;
	    second_pop34=pop3;
	  }

	  f_chain[(pop1*N_pops+pop2)*N_pops+pop3]
	    += N_nodes[pop4]
	    *(c[(pop2*N_pops+first_pop34)*N_pops+second_pop34]
	      *e[(pop1*N_pops+pop2)*N_pops+pop4]
	      +d[(first_pop14*N_pops+second_pop14)*N_pops+pop2]
	      *e[(pop4*N_pops+pop2)*N_pops+pop3]);
	  
	}
	// subtract off overcounting from when pop4 match pop1,pop2,pop3
	f_chain[(pop1*N_pops+pop2)*N_pops+pop3]
	  -= c[(pop2*N_pops+first_pop13)*N_pops+second_pop13]
	  *e[(pop1*N_pops+pop2)*N_pops+pop1]
	  +d[(pop1*N_pops+pop1)*N_pops+pop2]
	  *e[(pop1*N_pops+pop2)*N_pops+pop3];
	f_chain[(pop1*N_pops+pop2)*N_pops+pop3]
	  -= c[(pop2*N_pops+first_pop23)*N_pops+second_pop23]
	  *e[(pop1*N_pops+pop2)*N_pops+pop2]
	  +d[(first_pop12*N_pops+second_pop12)*N_pops+pop2]
	  *e[(pop2*N_pops+pop2)*N_pops+pop3];
	f_chain[(pop1*N_pops+pop2)*N_pops+pop3]
	  -= c[(pop2*N_pops+pop3)*N_pops+pop3]
	  *e[(pop1*N_pops+pop2)*N_pops+pop3]
	  +d[(first_pop13*N_pops+second_pop13)*N_pops+pop2]
	  *e[(pop3*N_pops+pop2)*N_pops+pop3];
      }
    }
  }
  

  indcount=0;

  // N_pops^2 f_diag's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++) {
      if(sigma[i*N_pops+j]) {
	gsl_vector_set(f,indcount, f_diag[i*N_pops+j]);
	indcount++;
      }
    }
  
  // N_pops*(N_pops+1)/2 f_recip's
  for(int i=0; i<N_pops; i++) 
    for(int j=i; j<N_pops; j++) {
      if(sigma[i*N_pops+j]*sigma[j*N_pops+i]) {
	gsl_vector_set(f,indcount, f_recip[i*N_pops+j]);
	indcount++;
      }
    }
  
  // N_pops^2*(N_pops+1)/2 f_conv's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++)
      for(int k=j; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[i*N_pops+k]) {
	  gsl_vector_set(f,indcount, f_conv[(i*N_pops+j)*N_pops+k]);
	  indcount++;
	}
      }

  // N_pops^2*(N_pops+1)/2 f_div's
  for(int i=0; i<N_pops; i++) 
    for(int j=i; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+k]*sigma[j*N_pops+k]) {
	  gsl_vector_set(f,indcount, f_div[(i*N_pops+j)*N_pops+k]);
	  indcount++;
	}
      }
  
  // N_pops^3 f_chain's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[j*N_pops+k]) {
	  gsl_vector_set(f,indcount, f_chain[(i*N_pops+j)*N_pops+k]);
	  indcount++;
	}
      }
  
  delete [] a;
  delete [] b;
  delete [] c;
  delete [] d;
  delete [] e;
  delete [] f_diag;
  delete [] f_recip;
  delete [] f_conv;
  delete [] f_div;
  delete [] f_chain;
  return GSL_SUCCESS;
}


int calc_sqrtcov_given_rhos_refine
(int N_pops, int *N_nodes, double *sigma, double *rho_recip, double *rho_conv, 
 double *rho_div, double *rho_chain, 
 double *sqrt_diag, double *sqrt_recip, double *sqrt_conv, 
 double *sqrt_div, double *sqrt_chain) {
  
  const gsl_multiroot_fsolver_type *T;
  gsl_multiroot_fsolver *s;
  
  int status;

  // count number of variables that aren't fixed at zero
  // due to the probability of an edge being zero (and hence sigma=0)
  // max possible: 2*gsl_pow_3(N_pops)+N_pops*(5*N_pops+1)/2;
  int N_vars = 0;
  for(int i=0; i<N_pops; i++)  {
    for(int j=0; j<N_pops; j++) {
      if(sigma[i*N_pops+j])
	N_vars++;
    }
    for(int j=i; j<N_pops; j++) {
      if(sigma[i*N_pops+j]*sigma[j*N_pops+i])
	N_vars++;
    }
    for(int j=0; j<N_pops; j++)
      for(int k=j; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[i*N_pops+k])
	  N_vars++;
      }
    for(int j=i; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+k]*sigma[j*N_pops+k])
	  N_vars++;
      }
    for(int j=0; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[j*N_pops+k])
	  N_vars++;
      }
  }


  cout << "Solving system of " << N_vars << " equations\n";
  cout.flush();
  
  struct pop_params p = { sigma,rho_recip, 
  			    rho_conv, rho_div,
  			     rho_chain, N_pops, N_nodes};


  gsl_multiroot_function f = {& sqrtcov_f, N_vars, &p};


  // set initial state based on the sqrt parameters passed into the function
  gsl_vector *x = gsl_vector_alloc (N_vars);

  int indcount=0;
  // N_pops^2 sqrt_diag's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++) {
      if(sigma[i*N_pops+j]) {
	gsl_vector_set(x,indcount, sqrt_diag[i*N_pops+j]);
	indcount++;
      }
    }

  // N_pops*(N_pops+1)/2 sqrt_recip's
  for(int i=0; i<N_pops; i++) 
    for(int j=i; j<N_pops; j++) {
      if(sigma[i*N_pops+j]*sigma[j*N_pops+i]) {
	gsl_vector_set(x,indcount, sqrt_recip[i*N_pops+j]);
	indcount++;
      }
    }
  
  // N_pops^2*(N_pops+1)/2 sqrt_conv's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++)
      for(int k=j; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[i*N_pops+k]) {
	  gsl_vector_set(x,indcount, sqrt_conv[(i*N_pops+j)*N_pops+k]);
	  indcount++;
	}
      }


  // N_pops^2*(N_pops+1)/2 sqrt_div's
  for(int i=0; i<N_pops; i++) 
    for(int j=i; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+k]*sigma[j*N_pops+k]) {
	  gsl_vector_set(x,indcount, sqrt_div[(i*N_pops+j)*N_pops+k]);
	  indcount++;
	}
      }

  // N_pops^3 sqrt_chain's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[j*N_pops+k]) {
	  gsl_vector_set(x,indcount, sqrt_chain[(i*N_pops+j)*N_pops+k]);
	  indcount++;
	}
      }
  
  T = gsl_multiroot_fsolver_hybrids;
  s = gsl_multiroot_fsolver_alloc (T, N_vars);
  gsl_multiroot_fsolver_set (s, &f, x);
  
  int iter=0;
  do
    {
      iter++;
      status = gsl_multiroot_fsolver_iterate (s);
      
      if (status)   /* check if solver is stuck */
	break;
      
      status =
	gsl_multiroot_test_residual (s->f, 1e-13);
    }
  while (status == GSL_CONTINUE && iter < 1000);
  
  cout << "After " << iter << " iterations of solver, square root refine status = "
       << gsl_strerror(status) << "\n";

  // cout << "f = ";
  // for(int i=0; i<n; i++) 
  //   cout << gsl_vector_get(s->f, i) << " ";
  // cout << "\n";

  // if had problems, don't modify values of sqrt parameter
  if(status) {
    gsl_multiroot_fsolver_free (s);
    gsl_vector_free (x);
    return(status);
  }
  
  // else set sqrt parameters to result of function
  indcount=0;

  // max N_pops^2 sqrt_diag's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++) {
      if(sigma[i*N_pops+j]) {
	sqrt_diag[i*N_pops+j] = gsl_vector_get(s->x,indcount);
	indcount++;
      }
      else {
	sqrt_diag[i*N_pops+j] = 0;
      }
    }

  // max N_pops*(N_pops+1)/2 sqrt_recip's
  for(int i=0; i<N_pops; i++) 
    for(int j=i; j<N_pops; j++) {
      if(sigma[i*N_pops+j]*sigma[j*N_pops+i]) {
	sqrt_recip[i*N_pops+j] = gsl_vector_get(s->x,indcount);
	indcount++;
      }
      else {
	sqrt_recip[i*N_pops+j] = 0;
      }
    }
  
  // max N_pops^2*(N_pops+1)/2 sqrt_conv's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++)
      for(int k=j; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[i*N_pops+k]) {
	  sqrt_conv[(i*N_pops+j)*N_pops+k] = gsl_vector_get(s->x,indcount);
	  indcount++;
	}
	else {
	  sqrt_conv[(i*N_pops+j)*N_pops+k] = 0;
	}
      }

  // max N_pops^2*(N_pops+1)/2 sqrt_div's
  for(int i=0; i<N_pops; i++) 
    for(int j=i; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+k]*sigma[j*N_pops+k]) {
	  sqrt_div[(i*N_pops+j)*N_pops+k] = gsl_vector_get(s->x,indcount);
	  indcount++;
	}
	else {
	  sqrt_div[(i*N_pops+j)*N_pops+k] = 0;
	} 
      }

  // max N_pops^3 sqrt_chain's
  for(int i=0; i<N_pops; i++) 
    for(int j=0; j<N_pops; j++)
      for(int k=0; k<N_pops; k++) {
	if(sigma[i*N_pops+j]*sigma[j*N_pops+k]) {
	  sqrt_chain[(i*N_pops+j)*N_pops+k] = gsl_vector_get(s->x,indcount);
	  indcount++;
	}
	else {
	  sqrt_chain[(i*N_pops+j)*N_pops+k] = 0;
	}
      }
  
  gsl_multiroot_fsolver_free (s);
  gsl_vector_free (x);

  return 0;
}
