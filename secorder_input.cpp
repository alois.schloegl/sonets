#include "secorder_input.hpp"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_blas.h>

using namespace std;

struct sqrt_params
{
  double sigma0, sigma1;
  double rho00, rho01, rho11;
  int N_nodes0, N_nodes1;
};


int sqrtcov_f(const gsl_vector * x, void *params,
	    gsl_vector * f) {

  double sigma0=((struct sqrt_params *) params)->sigma0;
  double sigma1=((struct sqrt_params *) params)->sigma1;
  double rho00=((struct sqrt_params *) params)->rho00;
  double rho01=((struct sqrt_params *) params)->rho01;
  double rho11=((struct sqrt_params *) params)->rho11;
  
  int N_nodes0=((struct sqrt_params *) params)->N_nodes0;
  int N_nodes1=((struct sqrt_params *) params)->N_nodes1;
  
  double sqrt_diag0 = gsl_vector_get(x,0);
  double sqrt_diag1 = gsl_vector_get(x,1);
  double sqrt_offdiag00 = gsl_vector_get(x,2);
  double sqrt_offdiag01 = gsl_vector_get(x,3);
  double sqrt_offdiag11 = gsl_vector_get(x,4);

  double temp;
  temp = sqrt_diag0*sqrt_diag0 
    +(N_nodes0-1)*sqrt_offdiag00*sqrt_offdiag00
    +N_nodes1*sqrt_offdiag01*sqrt_offdiag01
    -sigma0*sigma0;
  gsl_vector_set(f,0,temp);

  temp=sqrt_diag1*sqrt_diag1
    +(N_nodes1-1)*sqrt_offdiag11*sqrt_offdiag11
    +N_nodes0*sqrt_offdiag01*sqrt_offdiag01
    -sigma1*sigma1;
  gsl_vector_set(f,1,temp);
  
  temp = 2*sqrt_diag0*sqrt_offdiag00
    +(N_nodes0-2)*sqrt_offdiag00*sqrt_offdiag00
    +N_nodes1*sqrt_offdiag01*sqrt_offdiag01
    -sigma0*sigma0*rho00;
  gsl_vector_set(f,2,temp);

  temp = 2*sqrt_diag1*sqrt_offdiag11
    +(N_nodes1-2)*sqrt_offdiag11*sqrt_offdiag11
    +N_nodes0*sqrt_offdiag01*sqrt_offdiag01
    -sigma1*sigma1*rho11;
  gsl_vector_set(f,3,temp);

  temp = (sqrt_diag0+sqrt_diag1)*sqrt_offdiag01
    +(N_nodes0-1)*sqrt_offdiag00*sqrt_offdiag01
    +(N_nodes1-1)*sqrt_offdiag11*sqrt_offdiag01
    -sigma0*sigma1*rho01;
  gsl_vector_set(f,4,temp);

  return GSL_SUCCESS;

}



int secorder_input::setup_params() {
  sigma0 = 1.0/gsl_cdf_ugaussian_Qinv(input_p0);
  sigma1 = 1.0/gsl_cdf_ugaussian_Qinv(input_p1);

  rho00=calc_rho_given_alpha(input_p0,input_p0, 
			     input_corr0*(1.0/input_p0-1.0), 
			     status);
  if(status)
    return status;
  
  rho11=calc_rho_given_alpha(input_p1,input_p1, 
			     input_corr1*(1.0/input_p1-1.0), 
			     status);
  if(status)
    return status;
  
  rho01 = sqrt(rho00*rho11)*input_cc_01;

  
  
  // calculate square root assuming large N
  const size_t nmat=2;
  gsl_eigen_symmv_workspace *work_eig=gsl_eigen_symmv_alloc(nmat);
  
  gsl_matrix *A = gsl_matrix_alloc(nmat,nmat);    // the 4x4 covariance matrix
  gsl_matrix *sqrtA =gsl_matrix_alloc(nmat,nmat); // its square root
  gsl_vector *evals=gsl_vector_alloc(nmat);       // evals of A
  gsl_matrix *evecs=gsl_matrix_alloc(nmat,nmat);  // evects of A
  
  gsl_matrix_set(A,0,0, N_nodes[0]*sigma0*sigma0*rho00);
  gsl_matrix_set(A,1,0, sqrt(N_nodes[0]*(double)N_nodes[1])
		 *sigma0*sigma1*rho01);
  gsl_matrix_set(A,1,1, N_nodes[1]*sigma1*sigma1*rho11);

  // to calculate square root of A
  // 1. take it's eigen decomposition
  // 2. take the square root of its eigenvalues
  // 3. reconstruct with new eigenvalues to get a square root of A
  
  gsl_eigen_symmv(A, evals, evecs, work_eig);
    
  double max_eval=-1E100;
  double min_eval=1E100;
  for(size_t i=0; i<nmat; i++) {
    double the_eval = gsl_vector_get(evals,i);
    if(the_eval > max_eval)
      max_eval=the_eval;
    if(the_eval < min_eval)
      min_eval=the_eval;
    if(the_eval <= 0) {
      if(the_eval > -1E-7) {
	// allow eigenvalues to be slightly negative due to
	// roundoff error
	the_eval=0;
      }
      else {
	// if have a negative eigenvalue, can't take square root
	// system of equations does not have a real solution
	// (at least in limit of large N)
	cout << "Found a negative eval=" << the_eval << endl;
	
	gsl_eigen_symmv_free(work_eig);
	gsl_matrix_free(A);
	gsl_matrix_free(sqrtA);
	gsl_matrix_free(evecs);
	gsl_vector_free(evals);
	return -1;
      }
    }
    // scale eigenvector by fourth root of eval so 
    // reconstruction with be based on square root of eval
    gsl_vector_view col = gsl_matrix_column(evecs,i);
    gsl_vector_scale(&col.vector, sqrt(sqrt(the_eval)));
  }
  
  // cout << "min_eval = " << min_eval
  //      <<", max_eval = " << max_eval << "\n";
  // reconstruct to get sqrt A
  gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,evecs,evecs,0,sqrtA);
  
  // undo scaling to get elements of the square root of // original covariance matrix
  sqrt_offdiag[0][0] = gsl_matrix_get(sqrtA,0,0)/N_nodes[0];
  sqrt_offdiag[0][1] = gsl_matrix_get(sqrtA,1,0)
    /sqrt(N_nodes[0]*(double)N_nodes[1]);
  sqrt_offdiag[1][1] = gsl_matrix_get(sqrtA,1,1)/N_nodes[1];
  
  gsl_eigen_symmv_free(work_eig);
  gsl_matrix_free(A);
  gsl_matrix_free(sqrtA);
  gsl_matrix_free(evecs);
  gsl_vector_free(evals);

  sqrt_diag[0] = sigma0*sigma0-(N_nodes[0]-1)*sqrt_offdiag[0][0]*sqrt_offdiag[0][0]
    -N_nodes[1]*sqrt_offdiag[0][1]*sqrt_offdiag[0][1];
  if(sqrt_diag[0] <0) {
    cout << "Can't calculate sqrt_diag[0]" << endl;
    return(-1);
  }
  sqrt_diag[0]=sqrt(sqrt_diag[0]);

  sqrt_diag[1] = sigma1*sigma1-(N_nodes[1]-1)*sqrt_offdiag[1][1]*sqrt_offdiag[1][1]
    -N_nodes[0]*sqrt_offdiag[0][1]*sqrt_offdiag[0][1];
  if(sqrt_diag[1] <0) {
    cout << "Can't calculate sqrt_diag[1]" << endl;
    return(-1);
  }
  sqrt_diag[1]=sqrt(sqrt_diag[1]);

  
  // now refine for finite N
  const gsl_multiroot_fsolver_type *T;
  gsl_multiroot_fsolver *s;
  
  struct sqrt_params p = { sigma0, sigma1,
			   rho00, rho01, rho11,
			   N_nodes[0], N_nodes[1]};

  gsl_multiroot_function f = {&sqrtcov_f, 5, &p};


  // set initial state based on the large N values
  gsl_vector *x = gsl_vector_alloc(5);
  gsl_vector_set(x,0,sqrt_diag[0]);
  gsl_vector_set(x,1,sqrt_diag[1]);
  gsl_vector_set(x,2,sqrt_offdiag[0][0]);
  gsl_vector_set(x,3,sqrt_offdiag[0][1]);
  gsl_vector_set(x,4,sqrt_offdiag[1][1]);

  T = gsl_multiroot_fsolver_hybrids;
  s = gsl_multiroot_fsolver_alloc (T, 5);
  gsl_multiroot_fsolver_set (s, &f, x);
  
  int iter=0;
  do
    {
      iter++;
      status = gsl_multiroot_fsolver_iterate (s);
      
      if (status)   /* check if solver is stuck */
	break;
      
      status =
	gsl_multiroot_test_residual (s->f, 1e-13);
    }
  while (status == GSL_CONTINUE && iter < 1000);
  
  // cout << "After " << iter << " iterations of solver, square root refine status = "
  //      << gsl_strerror(status) << "\n";

  // cout << "f = ";
  // for(int i=0; i<n; i++) 
  //   cout << gsl_vector_get(s->f, i) << " ";
  // cout << "\n";

  // if had problems, don't modify values of sqrt parameter
  if(status) {
    gsl_multiroot_fsolver_free (s);
    gsl_vector_free (x);
    return(status);
  }
  
  sqrt_diag[0]=gsl_vector_get(s->x,0);
  sqrt_diag[1]=gsl_vector_get(s->x,1);
  sqrt_offdiag[0][0]=gsl_vector_get(s->x,2);
  sqrt_offdiag[0][1]=gsl_vector_get(s->x,3);
  sqrt_offdiag[1][0]=gsl_vector_get(s->x,3);
  sqrt_offdiag[1][1]=gsl_vector_get(s->x,4);
  
  gsl_multiroot_fsolver_free (s);
  gsl_vector_free (x);
    
  cout << "input_ps = " << input_p0 << ", " << input_p1 << endl;
  cout << "input_corrs = " << input_corr0 
       << ", " << input_corr1 << ", input_cc_01 = " << input_cc_01 << endl;
  // cout << "sigmas = " << sigma0 << ", " << sigma1 << endl;
  // cout << "sqrt_diags = " << sqrt_diag[0] << ", " << sqrt_diag[1] << endl;
  // cout << "sqrt_offdiags = " << sqrt_offdiag[0][0]
  //      << ", " << sqrt_offdiag[0][1] << ", " << sqrt_offdiag[1][1] << endl;

  return 0;
}


  

int secorder_input::sample(gsl_vector *inputs, gsl_rng *rng) {
  gsl_vector_set_zero(inputs);
  
  int stride = inputs->stride;

  double *thedata = inputs->data;

  int N_nodes_offset[2] = {0,N_nodes[0]*stride};

  for(int pop1=0; pop1<2; pop1++) {
    int toplim1=N_nodes[pop1]*stride+N_nodes_offset[pop1];
    for(int i1=N_nodes_offset[pop1]; i1< toplim1; i1+=stride) {
      double temp= gsl_ran_gaussian(rng,1.0);
      thedata[i1] += temp*sqrt_diag[pop1];
        for(int pop2=0; pop2<2; pop2++) {
	  double temp2 = temp*sqrt_offdiag[pop1][pop2];
	  if(temp2) {
	    int toplim2 = N_nodes[pop2]*stride+N_nodes_offset[pop2];
	    for(int i2=N_nodes_offset[pop2]; i2<toplim2; i2+=stride) {
	      if(i2 != i1)
		thedata[i2] += temp2;
	    }
	  }
	}
    }
  }
  int toplim = (N_nodes[0]+N_nodes[1])*stride;
  for(int i=0; i<toplim; i+=stride)
    thedata[i] = thedata[i] >1.0;
  
  
  return 0;
  
  
}

int secorder_input::setup_trains(double input_rate0_, double input_rate1_, 
				 double input_corr0_, double input_corr1_,
				 double input_cc_01_, double dt_, 
				 int N_nodes0_, int N_nodes1_) {

  dt_batch = dt_;
  input_rate0 = input_rate0_;
  input_rate1 = input_rate1_;
    
  // choose dt_internal to be 0.1/input_rate so that input_p = 0.1;
  if(input_rate1>input_rate0)
    dt_internal = 0.1/input_rate1;
  else 
    dt_internal = 0.1/input_rate0;
  
  // but make so dt_batch is multiple of dt_internal 
  N_intervals_batch = ceil(dt_batch/dt_internal);
  dt_internal = dt_batch / N_intervals_batch;
  cout << "dt_internal = " << dt_internal 
       << ", N_intervals_batch = " << N_intervals_batch << endl;
  
  set_stats(input_rate0*dt_internal,input_rate1*dt_internal, 
	    input_corr0_, input_corr1_, input_cc_01_,
	    N_nodes0_, N_nodes1_);

  if(input_trains)
    gsl_matrix_free(input_trains);
  input_trains= gsl_matrix_alloc(N_nodes[0]+N_nodes[1],N_intervals_batch+1);
    
  return 0;
}

gsl_matrix *secorder_input::sample_trains(double time, gsl_rng *rng) {

  // first sample in terms of zero and ones
  // input_trains will indicate if input occured in that time step
  for(int j=0; j<N_intervals_batch; j++) {
    gsl_vector_view inputs_tstep = gsl_matrix_column(input_trains,j);
    sample(&inputs_tstep.vector, rng);
    
    
  }
  // for(int i=0; i<N_nodes; i++) {
  //   for(int j=0; j<N_intervals_batch; j++) {
  //     cout << gsl_matrix_get(input_trains,i,j) << " ";
  //   }
  //   cout << endl;
  // }
  
  // rewrite input_trains to be time of each input
  // randomly sample the actual time in each interval
  for(int i=0; i<N_nodes[0]+N_nodes[1]; i++) {
    int N_inputs_node=0;
    for(int j=0; j<N_intervals_batch; j++) {
      if(gsl_matrix_get(input_trains,i,j)) {
	gsl_matrix_set(input_trains, i, N_inputs_node,
		       time + dt_internal*(j+gsl_rng_uniform(rng)));
	N_inputs_node++;
      }
    }
    // mark the end of inputs for each node by -1
    gsl_matrix_set(input_trains,i,N_inputs_node,-1);
    
  }

  return input_trains;
  
}
