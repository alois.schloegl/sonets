#include <gsl/gsl_rng.h>
#include <gsl/gsl_matrix.h>

gsl_matrix* secorder_gen(int N_pops, int *N_nodes, double *p,
			 double *alpha_recip, 
			 double *alpha_conv_hom, 
			 double *cc_conv_mixed,
			 double *alpha_div_hom,
			 double *cc_div_mixed,
			 double *cc_chain, gsl_rng *r);
