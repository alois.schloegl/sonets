#include <iostream>
#include <gsl/gsl_math.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_eigen.h>

#include "calc_sqrtcov_gen.hpp"
#include "calc_rhos.hpp"
//#include "calc_stats_2p.hpp"
#include "secorder_gen.hpp"

using namespace std;


// declare auxiliary function
int gen_corr_gaussian(int N_pops, int *N_nodes, 
		      double *sqrt_diag, double *sqrt_recip,
		      double *sqrt_conv, double *sqrt_div, 
		      double *sqrt_chain,
		      gsl_matrix *thevars, gsl_rng *r);


gsl_matrix* secorder_gen(int N_pops, int *N_nodes, double *p,
			 double *alpha_recip, 
			 double *alpha_conv_hom, 
			 double *cc_conv_mixed,
			 double *alpha_div_hom,
			 double *cc_div_mixed,
			 double *cc_chain, gsl_rng *r) {

  //int calc_covs=0;  // if nonzero, calculate covariance of Gaussian
  
  int print_p = 1; // print out target values of p 
  int print_alpha=0; // print out target values of alpha
  int print_rho = 0;    // print out values of rho
  int print_sqrt = 0;   // print out values of square root of covariance

  int status;
  int N_nodes_tot=0;
  for(int i=0; i<N_pops; i++) 
    N_nodes_tot += N_nodes[i];

  
  cout << "Beginning secorder_gen with N_pops = " << N_pops 
       << " and N_nodes_tot = " << N_nodes_tot << "\n";

  if(print_p) {
    cout << "p = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++)
	cout << p[i*N_pops+j] << " ";
      cout << "\n";
    }
    cout << "\n";
    cout.flush();
  }
  if(print_alpha) {
    cout << "alpha_recip = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=i; j<N_pops; j++)
	cout << alpha_recip[i*N_pops+j] << " ";
      cout << "\n";
    }
    cout << "\n";
    
    cout << "alpha_conv_hom = \n"; 
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++) 
	cout << alpha_conv_hom[i*N_pops+j] << " ";
      cout << "\n";
    }
    cout << "\n";
    cout << "cc_conv_mixed = \n"; 
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops-1; j++) {
	for(int k=j+1; k<N_pops; k++) 
	  cout << cc_conv_mixed[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    
    cout << "alpha_div_hom = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++) 
	cout << alpha_div_hom[i*N_pops+j] << " ";
      cout << "\n";
    }
    cout << "\n";
    cout << "cc_div_mixed = \n";
    for(int i=0; i<N_pops-1; i++) {
      for(int j=i+1; j<N_pops; j++) {
	for(int k=0; k<N_pops; k++)
	  cout << cc_div_mixed[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    
    cout << "cc_chain = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++) {
	for(int k=0; k<N_pops; k++)
	  cout << cc_chain[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    cout.flush();
  }

  //////////////////////////////////////////////////////////////
  // Step 1: Transform desired alphas for the Bernoulli matrix
  // into the required covariance structure (rhos) 
  // of the underlying Gaussian matrix
  // The rhos implicitly define the Gaussian's covariance matrix
  //////////////////////////////////////////////////////////////
  
  double *rho_recip=new double[N_pops*N_pops];
  double *rho_conv=new double[N_pops*N_pops*N_pops];
  double *rho_div=new double[N_pops*N_pops*N_pops];
  double *rho_chain=new double[N_pops*N_pops*N_pops];
  

  for(int i=0; i<N_pops; i++) {
    for(int j=i; j<N_pops; j++) {
      rho_recip[i*N_pops+j] 
	= calc_rho_given_alpha(p[i*N_pops+j], p[j*N_pops+i], 
			       alpha_recip[i*N_pops+j],status);
      if(status) {
	delete [] rho_recip;
	delete [] rho_conv;
	delete [] rho_div;
	delete [] rho_chain;
	return 0;
      }
    }
    for(int j=0; j<N_pops; j++) {
      rho_conv[(i*N_pops+j)*N_pops+j]
	= calc_rho_given_alpha(p[i*N_pops+j], p[i*N_pops+j], 
			       alpha_conv_hom[i*N_pops+j],status);
      if(status) {
	delete [] rho_recip;
	delete [] rho_conv;
	delete [] rho_div;
	delete [] rho_chain;
	return 0;
      }
    }
    for(int j=0; j<N_pops; j++) {
      rho_div[(i*N_pops+i)*N_pops+j]
	= calc_rho_given_alpha(p[i*N_pops+j], p[i*N_pops+j], 
			       alpha_div_hom[i*N_pops+j],status);
      if(status) {
	delete [] rho_recip;
	delete [] rho_conv;
	delete [] rho_div;
	delete [] rho_chain;
	return 0;
      }
    }
  }
 
  for(int i=0; i<N_pops; i++) {
    for(int j=0; j<N_pops-1; j++) 
      for(int k=j+1; k<N_pops; k++) {
	double temp = rho_conv[(i*N_pops+j)*N_pops+j]
	  *rho_conv[(i*N_pops+k)*N_pops+k];
	if(temp < 0) {
	  delete [] rho_recip;
	  delete [] rho_conv;
	  delete [] rho_div;
	  delete [] rho_chain;
	  return 0;
	}
	rho_conv[(i*N_pops+j)*N_pops+k]
	  = sqrt(temp)*cc_conv_mixed[(i*N_pops+j)*N_pops+k];
      }

    for(int j=i+1; j<N_pops; j++) 
      for(int k=0; k<N_pops; k++) {
	double temp = rho_div[(i*N_pops+i)*N_pops+k]
	  *rho_div[(j*N_pops+j)*N_pops+k];
	if(temp < 0) {
	  delete [] rho_recip;
	  delete [] rho_conv;
	  delete [] rho_div;
	  delete [] rho_chain;
	  return 0;
	}
	rho_div[(i*N_pops+j)*N_pops+k]
	  = sqrt(temp)*cc_div_mixed[(i*N_pops+j)*N_pops+k];
      }
    for(int j=0; j<N_pops; j++) 
      for(int k=0; k<N_pops; k++) {
	double temp = rho_conv[(j*N_pops+k)*N_pops+k]
	  * rho_div[(i*N_pops+i)*N_pops+j];
	if(temp < 0) {
	  delete [] rho_recip;
	  delete [] rho_conv;
	  delete [] rho_div;
	  delete [] rho_chain;
	  return 0;
	}
	rho_chain[(i*N_pops+j)*N_pops+k]
	  =  sqrt(temp)*cc_chain[(i*N_pops+j)*N_pops+k];
      }
  }
  
  if(print_rho) {
    cout << "rho_recip = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=i; j<N_pops; j++)
	cout << rho_recip[i*N_pops+j] << " ";
      cout << "\n";
    }
    cout << "\n";
    cout << "rho_conv = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++) {
	for(int k=j; k<N_pops; k++)
	  cout << rho_conv[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    cout << "rho_div = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=i; j<N_pops; j++) {
	for(int k=0; k<N_pops; k++)
	  cout << rho_div[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    cout << "rho_chain = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++) {
	for(int k=0; k<N_pops; k++)
	  cout << rho_chain[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    cout.flush();
  }

  ////////////////////////////////////////////////////////////
  // Step 2: Take the square root of the Gaussian's covariance matrix
  // 
  ////////////////////////////////////////////////////////////
  
  double *sqrt_diag=new double[N_pops*N_pops];
  double *sqrt_recip=new double[N_pops*N_pops];
  double *sqrt_conv=new double[N_pops*N_pops*N_pops];
  double *sqrt_div=new double[N_pops*N_pops*N_pops];
  double *sqrt_chain=new double[N_pops*N_pops*N_pops];

  double *sigma = new double[N_pops*N_pops];
  for(int i=0; i<N_pops; i++)
    for(int j=0; j<N_pops; j++) 
      sigma[i*N_pops+j] = 1.0/gsl_cdf_ugaussian_Qinv(p[i*N_pops+j]);

  status = calc_sqrtcov_given_rhos
    (N_pops, N_nodes, sigma, rho_recip, rho_conv, rho_div, rho_chain,
     sqrt_diag, sqrt_recip, sqrt_conv, sqrt_div, sqrt_chain);
  
  delete [] sigma;
  delete [] rho_recip;
  delete [] rho_conv;
  delete [] rho_div;
  delete [] rho_chain;
  

  if(status) {
    cerr << "Could not find real square root\n";
    delete [] sqrt_diag;
    delete [] sqrt_recip;
    delete [] sqrt_conv;
    delete [] sqrt_div;
    delete [] sqrt_chain;
    return 0;
  }

  if(print_sqrt) {
    cout << "sqrt_diag = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++)
	cout << sqrt_diag[i*N_pops+j] << " ";
      cout << "\n";
    }
    cout << "\n";
    cout << "sqrt_recip = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=i; j<N_pops; j++)
	cout << sqrt_recip[i*N_pops+j] << " ";
      cout << "\n";
    }
    cout << "\n";
    cout << "sqrt_conv = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++) {
	for(int k=j; k<N_pops; k++)
	  cout << sqrt_conv[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    cout << "sqrt_div = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=i; j<N_pops; j++) {
	for(int k=0; k<N_pops; k++)
	  cout << sqrt_div[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    cout << "sqrt_chain = \n";
    for(int i=0; i<N_pops; i++) {
      for(int j=0; j<N_pops; j++) {
	for(int k=0; k<N_pops; k++)
	  cout << sqrt_chain[(i*N_pops+j)*N_pops+k] << " ";
	cout << "\n";
      }
      cout << "\n";
    }
    cout.flush();
  }



  ////////////////////////////////////////////////////////////
  // Step 3: Use the square root of the covariance matrix
  // to generate the Gaussian matrix with the desired 
  // covariance structure.
  // Simply need to generate a vector of independent Gaussians
  // and multiply by the covariance matrix
  ////////////////////////////////////////////////////////////

  
  gsl_matrix *W_gaus = gsl_matrix_alloc(N_nodes_tot, N_nodes_tot);

  cout << "Generating gaussian matrix...";
  cout.flush();
  // calculate gaussian matrix
  gen_corr_gaussian(N_pops, N_nodes, sqrt_diag, sqrt_recip, sqrt_conv, 
		    sqrt_div, sqrt_chain, W_gaus, r);
  cout << "done\n";
  cout.flush();


  ////////////////////////////////////////////////////////////
  // Optional step 4: Calculate the covariance structure
  // of the Gaussian matrix 
  // Then, one can check program output to see if the
  // Gaussian matrix was generated correctly
  ////////////////////////////////////////////////////////////


  // if(calc_covs) {

  //   cout << "Calculating correlations...";
  //   cout.flush();
  //   double sigma[2][2];
  //   double cov_recip[2][2];
  //   double cov_conv[2][2][2];
  //   double cov_div[2][2][2];
  //   double cov_chain[2][2][2];
  //   double cov_other=0.0;

  //   calc_gaus_covs(W_gaus, N_nodes,
  // 		   sigma,cov_recip,
  // 		   cov_conv, cov_div,
  // 		   cov_chain,cov_other);

  //   cout << "done\n";

  //   cout << "sigma = ";
  //   for(int i=0; i<N_pops; i++)
  //     for(int j=0; j<N_pops; j++)
  // 	cout << sigma[i*N_pops+j] << " ";
  //   cout << "\n";
  //   cout << "cov_recip = ";
  //   for(int i=0; i<N_pops; i++)
  //     for(int j=i; j<N_pops; j++)
  // 	cout << cov_recip[i*N_pops+j] << " ";
  //   cout << "\n";
  //   cout << "cov_conv = ";
  //   for(int i=0; i<N_pops; i++)
  //     for(int j=0; j<N_pops; j++)
  // 	for(int k=j; k<N_pops; k++)
  // 	  cout << cov_conv[(i*N_pops+j)*N_pops+k] << " ";
  //   cout << "\n";
  //   cout << "cov_div = ";
  //   for(int i=0; i<N_pops; i++)
  //     for(int j=i; j<N_pops; j++)
  // 	for(int k=0; k<N_pops; k++)
  // 	  cout << cov_div[(i*N_pops+j)*N_pops+k] << " ";
  //   cout << "\n";
  //   cout << "cov_chain = ";
  //   for(int i=0; i<N_pops; i++)
  //     for(int j=0; j<N_pops; j++)
  // 	for(int k=0; k<N_pops; k++)
  // 	  cout << cov_chain[(i*N_pops+j)*N_pops+k] << " ";
  //   cout << "\n";
  //   cout << "cov_other = " << cov_other << "\n";

  //   cout.flush();
  // }

  ////////////////////////////////////////////////////////////
  // Step 5: Calculate Bernoulli matrix
  // Simply make the Bernoulli variable be 1 
  // if the Gaussian variable is greater than 1
  ////////////////////////////////////////////////////////////

  cout << "Generating Bernoulli matrix...";
  cout.flush();
  // calculate bernoulli matrix
  gsl_matrix *W_ber = gsl_matrix_alloc(N_nodes_tot, N_nodes_tot);

  for(int i=0; i<N_nodes_tot;i++) {
    for(int j=0; j<N_nodes_tot; j++) {
      gsl_matrix_set(W_ber,i,j,gsl_matrix_get(W_gaus,i,j)>1.0);
    }
  }

  // free Gaussian matrix
  gsl_matrix_free(W_gaus);


  cout << "done\n";


  delete [] sqrt_diag;
  delete [] sqrt_recip;
  delete [] sqrt_conv;
  delete [] sqrt_div;
  delete [] sqrt_chain;
 
  // return Bernoulli matrix
  return W_ber;


}


///////////////////////////////////////////////////////
// gen_corr_gaussian
// Generate correlated Gaussian given the square of the 
// covariance matrix determined by sqrtcov_pars
///////////////////////////////////////////////////////

int gen_corr_gaussian(int N_pops, int *N_nodes, 
		      double *sqrt_diag, double *sqrt_recip,
		      double *sqrt_conv, double *sqrt_div, 
		      double *sqrt_chain,
		      gsl_matrix *thevars, gsl_rng *r) {

  gsl_matrix_set_zero(thevars);
  size_t tda= thevars->tda;
    
  int *N_shift = new int[N_pops+1];
  N_shift[0]=0;
  for(int i=0; i<N_pops; i++)
    N_shift[i+1]=N_nodes[i]+N_shift[i];
  
  double *thedata = thevars->data;

  // generate N_nodes_tot*(N_nodes_tot-1) independent Gaussians
  // then multipy by square root of covariance matrix 
  // determined by sqrtcov
  for(int pop1=0; pop1<N_pops; pop1++) 
    for(int i=N_shift[pop1]; i<N_shift[pop1+1]; i++) {
      
      size_t i_tda=i*tda;
      
      for(int pop2=0; pop2<N_pops; pop2++) 
	for(int j=N_shift[pop2]; j<N_shift[pop2+1]; j++) {
	  // no connection from neuron onto itself
	  if(i==j)
	    continue;
	  
	  int j_tda=j*tda;
	  
	  double gaus_ind= gsl_ran_gaussian(r,1.0);
	  
	  int ind_comb_12=pop1*N_pops+pop2;

	  thedata[i_tda + j] += gaus_ind*sqrt_diag[ind_comb_12];
	 
	  if(pop2 < pop1)
	    thedata[j_tda + i] += gaus_ind*sqrt_recip[pop2*N_pops+pop1];
	  else
	    thedata[j_tda + i] += gaus_ind*sqrt_recip[ind_comb_12];
	  
	  for(int pop3=0; pop3<N_pops; pop3++) {

	    double temp_conv, temp_div, temp_chain1, temp_chain2;
	    int ind_comb_132 = (pop1*N_pops+pop3)*N_pops+pop2;
	    int ind_comb_123 = ind_comb_12*N_pops+pop3;

	    if(pop3 < pop2) {
	      temp_conv=gaus_ind*sqrt_conv[ind_comb_132];
	    }
	    else {
	      temp_conv=gaus_ind*sqrt_conv[ind_comb_123];
	    }
	    if(pop3 < pop1) {
	      temp_div=gaus_ind*sqrt_div[(pop3*N_pops+pop1)*N_pops+pop2];
	    }
	    else {
	      temp_div=gaus_ind*sqrt_div[ind_comb_132];
	    }
	    temp_chain1 = gaus_ind*sqrt_chain[ind_comb_123];
	    temp_chain2 = gaus_ind*sqrt_chain[ind_comb_132];
	    
	    if(temp_conv || temp_div || temp_chain1 || temp_chain2) {
	      int klim = N_shift[pop3+1];
	      for(int k=N_shift[pop3]; k<klim; k++) {		
		if(k==i || k==j)
		  continue;
		int k_tda=k*tda;
		
		thedata[i_tda + k] += temp_conv;
		thedata[k_tda + j] += temp_div;
		thedata[j_tda + k] += temp_chain1;
		thedata[k_tda + i] += temp_chain2;
	      }
	    }
	  }
	}
    }

  delete [] N_shift;
  return 0;
}
