#ifndef SOINPUT_HPP
#define SOINPUT_HPP
#include<iostream>
#include<gsl/gsl_math.h>
#include<gsl/gsl_cdf.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_rng.h>
#include<gsl/gsl_randist.h>
#include "calc_rhos.hpp"

class secorder_input  {
  int N_nodes[2];
  double input_p0, input_p1;
  double input_corr0, input_corr1, input_cc_01, ;
  double rho00, rho01, rho11;
  int status;
  double sigma0, sigma1;
  
  double sqrt_diag[2];
  double sqrt_offdiag[2][2];

  gsl_matrix *input_trains;
  double dt_batch, dt_internal;
  int N_intervals_batch;
  double input_rate0, input_rate1;

  int setup_params();

public:
  // secorder_input(double input_p_, double input_corr_, int N_nodes_) {
  //   set_stats(input_p_, input_corr_, N_nodes_);
  // }
  secorder_input() {
    input_trains=0;
  }

  secorder_input(double input_rate0_, double input_rate1_, 
		 double input_corr0_, double input_corr1_,
		 double input_cc_01_, double dt_, 
		 int N_nodes0_, int N_nodes1_) {
    input_trains=0;
    setup_trains(input_rate0_, input_rate1_,
		 input_corr0_,  input_corr1_,  input_cc_01_, 
		 dt_, N_nodes0_, N_nodes1_);
  }
  ~secorder_input() {
    if(input_trains)
      gsl_matrix_free(input_trains);
  }

  int set_stats(double input_p0_, double input_p1_, 
		double input_corr0_, double input_corr1_,
		double input_cc_01_,
		int N_nodes0_=0, int N_nodes1_=0) {
    if (input_p0_>1) {
	std::cerr<<"secorder_input::set_stats: input probability 0 > 1 !!"<<std::endl;
	exit(-1);
    }
    if (input_p1_>1) {
	std::cerr<<"secorder_input::set_stats: input probability 1 > 1 !!"<<std::endl;
	exit(-1);
    }
    input_p0 = input_p0_;
    input_p1 = input_p1_;
    input_corr0 = input_corr0_;
    input_corr1 = input_corr1_;
    input_cc_01 = input_cc_01_;
    if(N_nodes0_) 
      N_nodes[0] = N_nodes0_;
    if(N_nodes1_) 
      N_nodes[1] = N_nodes1_;
    status=setup_params();

    if(status) {
      std::cerr << "secorder_input::set_stats: failed" << std::endl;
      exit(-1);
    }
    return 0;
  }

  int sample(gsl_vector *inputs, gsl_rng *rng);

  int setup_trains(double input_rate0_, double input_rate1_, 
		   double input_corr0_, double input_corr1_,
		   double input_cc_01_, double dt_, 
		   int N_nodes0_, int N_nodes1_);

  gsl_matrix *sample_trains(double time, gsl_rng *rng);
};


#endif
