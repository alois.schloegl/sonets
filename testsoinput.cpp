#include <iostream>
#include <fcntl.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>
#include "secorder_input.hpp"

using namespace std;

int main(int argc, char *argv[]) {
  
  gsl_rng *rng = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_rng_set(rng, 47);

  int N_nodes0=2000, N_nodes1=500;
  double mean_rate0=10.0;
  double mean_rate1=5.0;
  double input_corr0=0.02;
  double input_corr1=0.02;
  double input_cc_01=-1.0;

  double dt = 2.0;
  //double tmax=2.0;
  
  
  secorder_input cinput;

  cinput.setup_trains(mean_rate0,mean_rate1,
		      input_corr0,input_corr1,input_cc_01,
		      dt, N_nodes0, N_nodes1);
  
  
  //gsl_vector *the_inputs =gsl_vector_alloc(N_nodes);

  FILE *fhnd;
  fhnd = fopen("testinput", "w");
  if(fhnd==NULL) {
    cerr << "Couldn't open testinput\n";
    exit(-1);
  }
  
  gsl_matrix *the_inputs;
  the_inputs = cinput.sample_trains(100, rng);
  // gsl_matrix_fprintf (fhnd, the_inputs, "%e");

  for(size_t j=0; j< the_inputs->size2; j++) {
    for(int i=0; i<N_nodes0+N_nodes1;i++) {
      fprintf(fhnd, "%f ", gsl_matrix_get(the_inputs,i,j));
    }
    fprintf(fhnd,"\n");
  }

  fclose(fhnd);
  

  
  return 0;
}
